/**
 * @file tim.h
 * @author
 * @date
 * @brief
 * 
 */

#ifndef C4MLIB_HARDWARE2_TIM_H
#define C4MLIB_HARDWARE2_TIM_H

#include "c4mlib/interrupt/src/isr_func.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct __tim_set_str {
    uint8_t ClockSource0;
    uint8_t ClockSource1;
    uint8_t WaveMode0;
    uint8_t Waveout0;
    uint8_t Waveout1;
    uint8_t Waveout2;
    uint8_t PWMIntEn;
    uint8_t TimIntEn;
    uint8_t WaveoutPin0;
    uint8_t WaveoutPin1;
    uint8_t WaveoutPin2;
    uint8_t TimerPeriod;

} TimSetStr_t;

typedef struct __tim_int_str {
    TimSetStr_t TimSet;

    uint8_t (*SetFunc_p)(struct __tim_int_str *);

    uint8_t IntTotal;                                ///< 紀錄已註冊多少中斷
    volatile FuncBlockStr_t IntFb[MAX_TIM_FUNCNUM];  ///< 紀錄所有已註冊的中斷函式
} TimIntStr_t;

uint8_t TimInt_net(TimIntStr_t *TimIntStr_p, uint8_t Num);

uint8_t TimInt_set(TimIntStr_t *IntStr_p);

uint8_t TimInt_reg(TimIntStr_t *TimIntStr_p, Func_t FbFunc_p, void *FbPara_p);

void TimInt_en(TimIntStr_t *TimIntStr_p, uint8_t Fb_Id, uint8_t enable);

void TimInt_step(TimIntStr_t *TimIntStr_p);
/* Public Section End */

/* Public Section Start */
/*----- Tim Sets Macros -----------------------------------------------------*/

#define DISABLE 0
#define ENABLE  1
#define OUTPUT  1
#define INPUT   0

/* ClockSource0 */
#define TIM_CLOCKSOURCE_CLK_IO          0
#define TIM_CLOCKSOURCE_CLK_OSC         1
/* ClockSource1 */ 
#define TIM0_CLOCKPRESCALEDBY_1         1
#define TIM0_CLOCKPRESCALEDBY_8         2
#define TIM0_CLOCKPRESCALEDBY_32        3
#define TIM0_CLOCKPRESCALEDBY_64        4
#define TIM0_CLOCKPRESCALEDBY_128       5
#define TIM0_CLOCKPRESCALEDBY_256       6
#define TIM0_CLOCKPRESCALEDBY_1024      7
#define TIM123_CLOCKPRESCALEDBY_1       1
#define TIM123_CLOCKPRESCALEDBY_8       2
#define TIM123_CLOCKPRESCALEDBY_64      3
#define TIM123_CLOCKPRESCALEDBY_256     4
#define TIM123_CLOCKPRESCALEDBY_1024    5
/* WaveMode0 */ 
#define WAVEMODE_SQUARE_FIXED           0
#define WAVEMODE_SQUARE_ADJUSTABLE      1
/* Waveout0 */ // DISABLE, ENABLE 
/* Waveout1 */ // DISABLE, ENABLE 
/* Waveout2 */ // DISABLE, ENABLE 
/* PWMIntEn */ // DISABLE, ENABLE 
/* TimIntEn */ // DISABLE, ENABLE 
/* WaveoutPin0 */ // OUTPUT, INPUT 
/* WaveoutPin1 */ // OUTPUT, INPUT 
/* WaveoutPin2 */ // Output, Input 
/* TimerPeriod */ // {f_clkIO} over {2N f_OCn} -1

/*---------------------------------------------------------------------------*/
/* Public Section End */

#endif  // C4MLIB_HARDWARE2_TIM_H
