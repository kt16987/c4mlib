/**
 * @file test_slave_spi.c
 * @author Deng Xiang-Guan
 * @date 2019.10.04
 * @brief 測試SPI Slave IO腳與通訊正常與否。
 * 
 * 需搭配test_master_spi.c，測試SPI Slave僕)接收與傳輸資料，與GPIO正常與否
 * ，測試方式如下條列表示：
 *   1. 初始化IO，並打出High和Low訊號給Master端，如果測試都正常Master會回覆
 *      ，程式繼續往下測試。
 *   2. 測試SPI暫存器設定是否正確，呼叫硬體SPI控制暫存器，檢查bit是否有成功寫
 *      入，成功程式繼續往下測試。
 *   3. 測試SPI通訊，Master端會和Slave通訊，如果測試資料傳輸過去與接收回來，
 *      都成功，程式繼續往下測試。
 *   4. 測試ASA_SPIM_trm和ASA_SPIM_rec mode 0，Slave端的狀態機會去處理
 *      Master端的資料，接收與發送和測試資料都正確，此測試程式結束。
 */

#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/time/src/hal_time.h"

#include <stdlib.h>
#define SPI_TEST_OK 0
#define SPI_TEST_FAIL 1

// Test function
char test_Slave_SPI_IO(void);
char test_Slave_SPI_Reg(void);
char test_Slave_SPI_transmission(void);
char test_Slave_SPI_reciept(void);
void init_timer(void);
uint8_t read_SPIData();

// Use DIO3(PF7)、DIO2(PF6) to keep slave wait master.
// When master let PF7 LOW, then start slave,
// it can let each other almost run together.
void wait_M_init(void);
bool wait_M_OK(void);

uint32_t last_time = 0;
ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

typedef struct SlaveDataStruct {
    uint16_t datalen;

} SlaveDataStruct;

uint8_t test_transmit[8] = {87, 11, 22, 33, 55, 66, 0, 0};
uint8_t testData[8];
uint8_t cnt = 0;
uint8_t fg = 0;
uint8_t tmp = 0;
static unsigned char chk = 0, wr = 0, bytescount = 0, state = 0, k = 0;
void *Data_p, *data_tmp_p, *echo_p;
uint32_t a = 4293922695;
bool ckflag = false;

ISR(SPI_STC_vect) {
    if (fg == 0) {
        uint8_t data = SPDR;
        if (data == 87) {
            printf("Recieve[%d] : %d\n", cnt, data);
            cnt++;
        }
        else if (cnt > 0) {
            testData[cnt] = data;
            printf("Recieve[%d] : %d\n", cnt, data);
            cnt++;
            if (cnt == 8)
                cnt = 0;
        }
    }
    else {
        switch (state) {
            case 0:
                wr = SPDR;
                // printf("=>%d\n", wr);
                if (wr & 0x80) {
                    state = 1;  // master send slave get
                    bytescount = 0;
                    k = 0;
                    chk = 0;
                    SPDR = SPDR;
                }
                else {
                    state = 2;  // master get slave send
                    a = 4293922695;
                    Data_p =
                        (void *)&a;  // ff f0 0f 87, 十進制:255 240 15 135，LSB
                    echo_p = (void *)&a;
                    bytescount = 0;
                    k = 0;
                    chk = 0;
                    SPDR = *((unsigned char *)Data_p + bytescount);
                    bytescount++;
                }
                break;
            case 1:
                if (bytescount < 4) {
                    *((unsigned char *)data_tmp_p + bytescount) = SPDR;
                    bytescount++;
                    state = 1;
                    SPDR = SPDR;
                }
                else if (bytescount == 4) {
                    bytescount++;
                    state = 1;
                    SPDR = SPDR;
                }
                else if (bytescount == 5) {
                    chk = SPDR;
                    if (chk == 0) {
                        for (int i = 0; i < 4; i++) {
                            *((unsigned char *)Data_p + i) =
                                *((unsigned char *)data_tmp_p + i);
                        }
                    }
                    state = 0;
                    SPDR = 0XAA;
                }
                break;
            case 2:
                wr = SPDR;
                if (bytescount == 1) {
                    SPDR = *((unsigned char *)Data_p + bytescount);
                    bytescount++;
                }
                else if (bytescount < 4) {
                    if (wr != *((unsigned char *)echo_p + bytescount - 2)) {
                        chk = 1;
                    }
                    SPDR = *((unsigned char *)Data_p + bytescount);
                    bytescount++;
                }
                else if (bytescount == 4) {
                    if (wr != *((unsigned char *)echo_p + bytescount - 2)) {
                        chk = 1;
                    }
                    SPDR = 87;
                    bytescount++;
                }
                else if (bytescount == 5) {
                    if (wr != *((unsigned char *)echo_p + bytescount - 2)) {
                        chk = 1;
                    }
                    SPDR = chk;
                    bytescount++;
                }
                else if (bytescount == 6) {
                    state = 0;
                    SPDR = 0xaa;
                    ckflag = true;
                }
                // printf("SPDR:%d, bytescount:%d\n", wr, bytescount);
                break;

            default:
                break;
        }

        // printf("%d, %d\n", state, chk);
    }
}

int main() {
    // Setup
    C4M_STDIO_init();
    HAL_time_init();
    init_timer();
    sei();
    data_tmp_p = (void *)malloc(8);
    Data_p = (void *)malloc(8);
    uint16_t er_cnt = 0;
    bool er_flag = false;
    last_time = HAL_get_time();
    wait_M_init();
    printf("Start slave ~\n");
    // Main code
    while (true) {
        /**
         * Test1
         * @brief:test spi IO.
         */
        printf("\n\n\n====================================\n");
        printf("Start test slave SPI IO ...\n");
        while (test_Slave_SPI_IO() != SPI_TEST_OK) {
            printf("PINB0_2 : %d, DDRB: %d, PORTB : %d\n", PINB & 0x07, DDRB,
                   PORTB);
            if (HAL_get_time() - last_time > 500) {
                printf("Test slave SPI IO error\n");
                er_cnt++;
                if (er_cnt > 10) {
                    printf("Error break.\n");
                    er_cnt = 0;
                    er_flag = true;
                    break;
                }
                last_time = HAL_get_time();
            }
        }
        if (er_flag) {
            printf("test_Slave_SPI_IO Fail !!! \n\n");
            er_flag = false;
            while (true)
                ;
        }
        else {
            printf("test_Slave_SPI_IO Success !!! \n");
        }
        printf("====================================\n");

        /**
         * Test2
         * @brief:test spi register setting.
         */
        printf("\n\n\n====================================\n");
        printf("Start test slave SPI register setting ...\n");
        SPIS_Inst.init();
        while (test_Slave_SPI_Reg() != SPI_TEST_OK) {
            printf("Test slave SPI register error\n");
            printf("DDRB : %x\n", DDRB);
            printf("SPCR : %x\n", SPCR);
            printf("DDRF : %x\n", DDRF);
            if (HAL_get_time() - last_time > 500) {
                printf("Test slave SPI IO error\n");
                er_cnt++;
                if (er_cnt > 10) {
                    printf("Error break.\n");
                    er_cnt = 0;
                    er_flag = true;
                    break;
                }
                last_time = HAL_get_time();
            }
        }
        if (er_flag) {
            printf("Test_Slave_SPI_Reg Fail !!! \n\n");
            er_flag = false;
        }
        else {
            printf("Test_Slave_SPI_Reg Success !!! \n");
        }
        printf("====================================\n");

        /**
         * Test3
         * @brief:test Slave SPI reciept
         */
        SPCR |= (1 << SPIE);
        sei();
        printf("\n\n\n====================================\n");
        printf("Start test slave SPI transmission & reciept ...\n");
        char runTimes = 0;
        while (test_Slave_SPI_reciept() != SPI_TEST_OK || (++runTimes) <= 10) {
            if (HAL_get_time() - last_time > 500) {
                printf("Test slave SPI transmission & reciept error\n");
                er_cnt++;
                if (er_cnt > 10) {
                    printf("Error break.\n");
                    er_cnt = 0;
                    er_flag = true;
                    break;
                }
                last_time = HAL_get_time();
            }
        }
        if (er_flag) {
            printf("test_Slave_SPI_reciept Fail !!! \n\n");
            er_flag = false;
            while (true)
                ;
        }
        else {
            printf("test_Slave_SPI_reciept Success !!!\n");
        }
        printf("====================================\n");

        printf("Slave Test OK.\n");
        printf("\n\n\n====================================\n");
        printf("Test Mode Start.\n");

        while (!wait_M_OK())
            ;  // Keep unitl master ok
        wait_M_init();
        fg = 1;
        // Run state machine
        char rec_ans[4] = {10, 11, 12, 13};
        char ok_cnt = 0;
        uint32_t ccc = 0;
        while (true) {
            if (ckflag) {
                for (uint8_t i = 0; i < 4; i++) {
                    printf("%d,%d\n", ((uint8_t *)data_tmp_p)[i],
                           ((uint8_t *)Data_p)[i]);
                    if (((uint8_t *)data_tmp_p)[i] != rec_ans[i]) {
                        chk = 2;
                    }
                    rec_ans[i] += 4;
                }
                printf("\n\n");
                if (chk == 0) {
                    printf("Slave test mode 0 succeed!!! ->%d\n", ok_cnt);
                    ok_cnt++;
                }
                else {
                    printf("Slave test mode fail!!!\n");
                }
                // over 5 times ok
                if (ok_cnt > 5) {
                    break;
                }
                ckflag = false;
            }
            ccc++;
            if (ccc > 400000000) {
                printf("FFFF\n");
            }
        }
        printf("Slave Test OK.\n");
        printf("====================================\n");
        while (true)
            ;
    }
    return 0;
}

char test_Slave_SPI_IO(void) {
    static char toggle_flag = 1;
    static char io_cnt = 0;
    static uint16_t tmp_cnt = 0;
    static bool ok_f = false;
    BUS_SPI_DDR &=
        ~(1 << BUS_SPI_MOSI) & ~(1 << BUS_SPI_SCK) & ~(1 << BUS_SPI_SS);
    BUS_SPI_DDR |= (1 << BUS_SPI_MISO);

    if (toggle_flag) {
        BUS_SPI_PORT |= (1 << BUS_SPI_MISO);
    }
    else {
        BUS_SPI_PORT &= ~(1 << BUS_SPI_MISO);
    }
    toggle_flag ^= 1;
    if ((PINB & 0x07) == 0) {
        io_cnt++;
    }
    else if ((PINB & 0x07) == 7 && io_cnt == 1) {
        // wait master io test OK
        ok_f = true;
    }
    else {
        io_cnt = 0;
    }
    tmp_cnt++;
    if (ok_f == true && tmp_cnt > 100) {
        return SPI_TEST_OK;
    }
    return SPI_TEST_FAIL;
}

char test_Slave_SPI_Reg(void) {
    // 0 is input pin
    // 1 is output pin
    if (((BUS_SPI_DDR & (1 << BUS_SPI_MOSI)) >> BUS_SPI_MOSI) == 1)
        return 2;
    if (((BUS_SPI_DDR & (1 << BUS_SPI_SCK)) >> BUS_SPI_SCK) == 1)
        return 3;
    if (((BUS_SPI_DDR & (1 << BUS_SPI_SS)) >> BUS_SPI_SS) == 1)
        return 4;
    if (((BUS_SPI_DDR & (1 << BUS_SPI_MISO)) >> BUS_SPI_MISO) == 0)
        return 5;
    return SPI_TEST_OK;
}

char test_Slave_SPI_transmission(void) {
    return SPI_TEST_OK;
}

char test_Slave_SPI_reciept(void) {
    uint16_t c = 0;
    for (uint8_t i = 0; i < sizeof(test_transmit); i++) {
        if (testData[i] == test_transmit[i]) {
            c++;
        }
    }
    if (c == 7) {
        return SPI_TEST_OK;
    }
    else {
        return SPI_TEST_FAIL;
    }
}

void init_timer(void) {
    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}

uint8_t read_SPIData() {
    while (!(SPSR & (1 << SPIF)))
        ;
    return SPDR;
}

void wait_M_init(void) {
    DDRF |= (1 << PF6);
    DDRF &= ~(1 << PF7);
    PORTF = 0;
}
bool wait_M_OK(void) {
    PORTF |= (1 << PF6);
    if (PINF & (1 << PF7)) {
        printf("Start testing mode\n");
        return true;
    }
    return false;
};
