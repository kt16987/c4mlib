/**
 * @file test_twi_master_mode1.c
 * @author YuChen
 * @brief TWI Master mode1 測試程式
 * @date 2019.10.03
 *
 * 測試方式：<br>
 *      軟體：本測試程式需搭配test_twi_Slave_mode1.c同步測試。<br>
 *      硬體：須將兩塊ASA_M128單板電腦的SCL與SDA串接。<br>
 * 測試時須先將已燒錄test_twi_slave_mode1.c的ASA_M128啟動，再將燒錄test_twi_master_mode1.c的ASA_M128啟動。<br>
 *
 * 測試程式執行動作：<br>
 * 將欲設資料傳入Slave板中，並讀回經位移過Control Flag 後的值。<br>
 * 傳輸資料：已矩陣存取數值10~20。<br>
 * 接收資料：經Control Flag位移後的值。
 * 
 * 測試結果：<br>
 * >>  [MASTER] Transmit mode1 data1[10] = 20<br>
 * >>  [MASTER] Transmit mode1 data1[9] = 19<br>
 * >>  [MASTER] Transmit mode1 data1[8] = 18<br>
 * >>  [MASTER] Transmit mode1 data1[7] = 17<br>
 * >>  [MASTER] Transmit mode1 data1[6] = 16<br>
 * >>  [MASTER] Transmit mode1 data1[5] = 15<br>
 * >>  [MASTER] Transmit mode1 data1[4] = 14<br>
 * >>  [MASTER] Transmit mode1 data1[3] = 13<br>
 * >>  [MASTER] Transmit mode1 data1[2] = 12<br>
 * >>  [MASTER] Transmit mode1 data1[1] = 11<br>
 * >>  [Master] Error code : 0<br>
 * >>  [Master] Error code : 0<br>
 * >>  [Master] Receive mode1 data1[0] = 98<br>
 * >>  [Master] Receive mode1 data1[1] = 129<br>
 * >>  [Master] Receive mode1 data1[2] = 161<br>
 * >>  [Master] Receive mode1 data1[3] = 193<br>
 * >>  [Master] Receive mode1 data1[4] = 225<br>
 * >>  [Master] Receive mode1 data1[5] = 1<br>
 * >>  [Master] Receive mode1 data1[6] = 34<br>
 * >>  [Master] Receive mode1 data1[7] = 66<br>
 * >>  [Master] Receive mode1 data1[8] = 98<br>
 * >>  [Master] Receive mode1 data1[9] = 130<br>
 * >>  [Master] Receive mode1 data1[10] = 66
 */

#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/device/src/device.h"

#define ARRARY_SIZE 11
#define SLA 0x39
#define WAITTICK 50

void TWI_Master_set();
char check = 0;
int main(void) {
    C4M_STDIO_init();
    TWI_Master_set();
    uint8_t data1[ARRARY_SIZE];
    uint8_t bits1 = 0;
    uint8_t bits2 = 0;
    //  設定data初值
    for (int i = ARRARY_SIZE - 1; i >0; --i) {
        data1[i] = i + 10;
        printf("[MASTER] Transmit mode1 data1[%d] = %d\t\n", i, data1[i]);
    }
    //  將data全部向右位移3bits給Contral Flag使用
    for (int i = ARRARY_SIZE - 1; i >= 0; --i) {
        bits1 = data1[i] & 0x07;
        data1[i] >>= 3;
        data1[i] |= bits2 << 5;
        bits2 = bits1;
    }
    uint8_t Mode = 1;
    uint8_t regadd1 = 0x40;
    uint8_t bytes = ARRARY_SIZE;
    check = TWIM_trm(Mode, SLA, regadd1, bytes, data1, WAITTICK);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(150);
    check = TWIM_rec(Mode, SLA, regadd1, bytes, data1, WAITTICK);
    printf("[Master] Error code : %d\n", check);
    _delay_ms(150);
    for (int i = 0; i < bytes; i++) {
        printf("[Master] Receive mode1 data1[%d] = %d\t\n", i, data1[i]);
    }
}
void TWI_Master_set() {
    // Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ;
    // prescaler bits = 1
    TWBR = 12;
}
