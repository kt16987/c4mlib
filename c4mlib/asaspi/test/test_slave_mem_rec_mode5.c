/**
 * @file test_master_mem_trm_mode5.c
 * @author Deng Xiang-Guan
 * @date 2019.10.03
 * @brief 測試SPIM_Mem_rec mode 5函式。
 * 
 * 需搭配test_master_mem_rec_mode5.c，測試SPI Master(主)記憶接收資料，
 * 由低到高，回覆來自Master端的資料，測試方式如下條列表示：
 *   1. 是否發生SPI中斷，條件為SPI Master端 chip select為Low觸發。
 *   2. 發生中斷後，檢查Master傳送的command、memery address，如果都正確
 *      ，回覆資料。
 *   3. 回流程1。
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"

#define test_command 0x02

uint32_t mem_addr = 12341487;

typedef struct SPI_Mem_trm_test {
    volatile uint8_t index;
    volatile uint8_t command;
    volatile uint32_t reg_addr;
    volatile uint8_t data[5];
}test_spi_mem_t;

test_spi_mem_t mem_str = {0, 0, 0, {0, 1, 2, 3, 4}};

ISR(SPI_STC_vect) {
    uint8_t data = SPIS_Inst.read_byte();
    if(mem_str.index == 0) {
        mem_str.command = data;
    }
    else if(mem_str.index < 5) {
        SPIS_Inst.write_byte(mem_str.data[0]);
        if(mem_str.index == 1) {
            mem_str.reg_addr = data;
        }
        else {
            mem_str.reg_addr += (uint32_t)data << (8*(mem_str.index-1));
        }
        
        if(mem_str.index == 4) {
            mem_str.data[0] += sizeof(mem_str.data);
        }
    }
    else if((mem_str.index < 10)) {
        SPIS_Inst.write_byte(mem_str.data[mem_str.index-4]);
        mem_str.data[mem_str.index-4] += sizeof(mem_str.data);
    }
    mem_str.index++;
    if(mem_str.index == 10) {
        mem_str.index = 0;
    }
}

int main() {
    // Setup
    C4M_STDIO_init();
    printf("Start test SPIS_Mem_rec mode 5\n");
    SPIS_Inst.init();
    sei();
    _delay_ms(3000);
    while(true) {
        cli();
        printf("cmd:%x, regAdd:%lu, data[0]:%d, data[1]:%d, data[2]:%d, data[3]:%d, data[4]:%d\n", 
                mem_str.command, mem_str.reg_addr, mem_str.data[0], mem_str.data[1], mem_str.data[2], mem_str.data[3], mem_str.data[4]);
        sei();
        _delay_ms(3000);

    }

}
