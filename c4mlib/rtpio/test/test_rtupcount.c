/**
 * @file test_rtupcount.c
 * @author Yi-Mou
 * @brief realtimeupcount實作
 * @date 2019-08-26
 *
 * 紀錄timer中斷發生次數，計算出秒數
 */

#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/tim.h"
#include "c4mlib/rtpio/src/rtupcount.h"

#include <avr/io.h>

RealTimeUpCountStr_t RealTimeUpCount_1 = {0};
TimIntStr_t TimInt2 = TIMINT_2_STR_INI;

void init_timer2();  // timer2 CTC模式 波型頻率:100Hz

int main() {
    C4M_DEVICE_set();
    TimInt_net(&TimInt2, 2);

    RealTimeUpCount_1.Fb_Id =
        TimInt_reg(&TimInt2, (Func_t)RealTimeUpCount_step,
                   &RealTimeUpCount_1);  //將step函式註冊至timer2中斷
    TimInt_en(&TimInt2, RealTimeUpCount_1.Fb_Id, true);

    init_timer2();
    sei();

    uint8_t times = 0;
    DDRA = 0xFF;
    while (1) {
        if (RealTimeUpCount_1.TrigCount == 200) {
            times++;
            RealTimeUpCount_1.TrigCount = 0;
        }
        printf("%d\n", times);
    }
}

void init_timer2() {
    OCR2 = 53;
    TCCR2 = 0b000001101;
    TCNT2 = 0;
    TIMSK |= 1 << OCIE2;
}
