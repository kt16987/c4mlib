/**
 * @file 7s00.c
 * @author smallplayplay
 * @author YP-Shen
 * @author LiYu87
 * @date 2019.10.07
 * @brief 實現ASA 7s00驅動函式，提供使用者呼叫、設定及使用7s00的工作模式。
 *
 * LSByte
 * 200 :
 *     bit 7 ~ 4 : 分別代表位元 4 ~ 1 要不要閃爍
 *     bit 3 ~ 0 : 分別代表位元 4 ~ 1 要不要顯示小數點
 * 0 ~ 3 :
 *     分別代表位元 1 ~ 4 要顯示的字元
 */

#include "c4mlib/asa7s00/src/7s00.h"

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/hardware/src/uart.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/pgmspace.h>

#include "c4mlib/config/asamodule.cfg"

#define PAC_7S00_HEADER 0x7D
#define PAC_7S00_DEVICE 0x01
#define PAC_7S00_CMD    0x04
#define PAC_7S00_LENGTH 0x04

#define RES_ERROR_UNSUPPORTED_ASCII 0x05

#define PAC_7S00_MASK 0xFF

// 將ASCII轉換為7S00顯示字元代號
// 0xFF 代表錯誤
// 0x1E 代表不顯示
static const char ASCII_TO_7S00_CODE[128] __attribute__((progmem)) = {
    0x1E, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
//    40,   41,   42,   43,   44,   45,   46,   47,   48,   49,
//      ,     ,     ,     ,     ,  '-',     ,  '/',  '0',  '1',
    0xff, 0xff, 0xff, 0xff, 0xff, 0x1B, 0xff, 0xff, 0x00, 0x01,
//    50,   51,   52,   53,   55,   55,   56,   57,   58,   59,
//   '2',  '3',  '4',  '5',  '6',  '7',  '8',  '9',  ':',  ';',
    0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0xff, 0xff,
//    60,   61,   62,   63,   66,   65,   66,   67,   68,   69,
//   '<',  '=',  '>',  '?',  '@',  'A',  'B',  'C',  'D',  'E',
    0xff, 0xff, 0xff, 0xff, 0xff, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E,
//    70,   71,   72,   73,   74,   75,   76,   77,   78,   79,
//   'F',  'G',  'H',  'I',  'J',  'K',  'L',  'M',  'N',  'O',
    0x0F, 0x10, 0x11, 0x12, 0x13, 0xff, 0x14, 0xff, 0x15, 0x16,
//    80,   81,   82,   83,   84,   85,   86,   87,   88,   89,
//   'P',  'Q',  'R',  'S',  'T',  'U',  'V',  'W',  'X',  'Y',
    0x17, 0xff, 0xff, 0xff, 0xff, 0x18, 0xff, 0xff, 0xff, 0x19,
//    90,   91,   92,   93,   94,   95,   96,   97,   98,   99,
//   'Z',  '[',  '\',  ']',  '^',  '_',  '`',  'a',  'b',  'c',
    0xff, 0xff, 0xff, 0xff, 0xff, 0x1A, 0xff, 0x0A, 0x0B, 0x0C,
//   100,  101,  102,  103,  104,  105,  106,  107,  108,  109,
//   'd',  'e',  'f',  'g',  'h',  'i',  'j',  'k',  'l',  'm',
    0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0xff, 0x14, 0xff,
//   110,  111,  112,  113,  114,  115,  116,  117,  118,  119,
//   'n',  'o',  'p',  'q',  'r',  's',  't',  'u',  'v',  'w',
    0x15, 0x16, 0x17, 0xff, 0xff, 0x18, 0xff, 0x18, 0xff, 0xff,
//   120,  121,  122,  123,  124,  125,  126,  127,
//   'x',  'y',  'z',  '{',  '|',  '}',  '~',  ' ',
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

/**
 * @brief 經由ASA Bus的UART與7s00傳輸。
 *
 * @ingroup asa7s00_func
 * @param ASA_ID ASA介面卡的ID編號。
 * @param *Str_p ASA 7S00 參數結構。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *
 * 此函式為c4mlib函式庫內部使用，不開放給使用者使用。
 *
 * 透過ASA Bus的傳輸，將7s00裝置上的ASA ID以及7s00的參數結構輸進此函數
 * 中，即可完成7s00參數結構的相關使用設定及傳輸。
 */
static char UART_7S00_Transfer(char ASA_ID, Asa7s00Para_t* Str_p) {
    uint8_t tmp;
    uint8_t data[4];
    uint8_t chksum = 0;
    uint8_t chk[6] = {0xD7, 0xD7, 0xD7, 0x4F, 0x4B, 0x9A};
    uint8_t recieve[6];

    for (uint8_t i = 0; i < 4; i++) {
        tmp = Str_p->_7S00_put_reg[i];
        data[3 - i] = pgm_read_byte(&ASCII_TO_7S00_CODE[tmp]);
    }
    uint8_t ff_flags = Str_p->FFFlags;
    data[0] = data[0] + ((ff_flags & 0x80) << 0) + ((ff_flags & 0x08) << 3);
    data[1] = data[1] + ((ff_flags & 0x40) << 1) + ((ff_flags & 0x04) << 4);
    data[2] = data[2] + ((ff_flags & 0x20) << 2) + ((ff_flags & 0x02) << 5);
    data[3] = data[3] + ((ff_flags & 0x10) << 3) + ((ff_flags & 0x01) << 6);

    // select module
    ASABUS_ID_set(ASA_ID);

    // headers section
    ASABUS_UART_transmit(PAC_7S00_HEADER);
    ASABUS_UART_transmit(PAC_7S00_HEADER);
    ASABUS_UART_transmit(PAC_7S00_HEADER);

    // device section
    ASABUS_UART_transmit(PAC_7S00_DEVICE);
    chksum = PAC_7S00_DEVICE;

    // command section
    ASABUS_UART_transmit(PAC_7S00_CMD);
    chksum += PAC_7S00_CMD;

    // length section
    ASABUS_UART_transmit(PAC_7S00_LENGTH);
    chksum += PAC_7S00_LENGTH;

    // Data section
    // 每個 data 代表一顆七節館的顯示方法
    // 由左至右分別為 data 0, 1, 2, 3
    // bit 0~5 -> 7S00 要顯示的字元代號，詳見 ASCII_TO_7S00_CODE
    // bit 6   -> 閃爍
    // bit 7   -> 小數點
    ASABUS_UART_transmit(data[0]);  // data
    chksum += data[0];
    ASABUS_UART_transmit(data[1]);
    chksum += data[1];
    ASABUS_UART_transmit(data[2]);
    chksum += data[2];
    ASABUS_UART_transmit(data[3]);
    chksum += data[3];

    // chksum section
    ASABUS_UART_transmit(chksum);

    for (uint8_t i = 0; i < 6; i++) {
        recieve[i] = ASABUS_UART_receive();
    }

    // deselect module
    ASABUS_ID_set(0);

    // NOTE 回傳值不一定正確，需要查看7S00的MCU驅動
    for (uint8_t i = 0; i < 6; i++) {
        if (recieve[i] != chk[i]) {
            return HAL_OK;
        }
    }
    return HAL_OK;
}

char ASA_7S00_set(char ASA_ID, char LSByte, char Mask, char Shift, char Data,
                  Asa7s00Para_t* Str_p) {
    char chk = 0;
    ASABUS_ID_set(ASA_ID);  // select module
    if (ASA_ID > 0x07) {
        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
    }
    if (LSByte != 200) {
        return HAL_ERROR_ADDR;
    }
    if (Shift > 7) {
        return HAL_ERROR_SHIFT;
    }

    Data = (Data << Shift);
    Str_p->FFFlags = (Str_p->FFFlags & ~Mask) | (Data & Mask);
    chk = UART_7S00_Transfer(ASA_ID, Str_p);

    ASABUS_ID_set(0);  // unselect module
    return chk;
}

char ASA_7S00_put(char ASA_ID, char LSByte, char Bytes, void* Data_p,
                  Asa7s00Para_t* Str_p) {
    char chk;
    ASABUS_ID_set(ASA_ID);  // select module

    if (ASA_ID > 0x07) {
        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
    }
    if (LSByte > 3) {
        return HAL_ERROR_ADDR;
    }
    if ((LSByte + Bytes) > 4) {
        return HAL_ERROR_BYTES;
    }

    // check ascii is displayable
    for (uint8_t i = 0; i < Bytes; i++) {
        uint8_t num = ((uint8_t*)Data_p)[i];
        if (num >= 128) {
            return RES_ERROR_UNSUPPORTED_ASCII;
        }
        else if (pgm_read_byte(&ASCII_TO_7S00_CODE[num]) == 0xff) {
            return RES_ERROR_UNSUPPORTED_ASCII;
        }
    }

    uint8_t i = 0;
    while ((LSByte + i) < (LSByte + Bytes)) {
        Str_p->_7S00_put_reg[LSByte + i] = ((unsigned char*)Data_p)[i];
        i++;
    }

    chk = UART_7S00_Transfer(ASA_ID, Str_p);

    ASABUS_ID_set(0);  // unselect module
    return chk;
}

char ASA_7S00_get(char ASA_ID, char LSByte, char Bytes, void* Data_p,
                  Asa7s00Para_t* Str_p) {
    int i = 0;
    if (ASA_ID > 0x07) {
        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
    }

    /* get set reg. */
    if (LSByte > 3) {
        if (LSByte != 200)
            return 2;
        if (Bytes != 1)
            return 3;
        *(char*)Data_p = Str_p->FFFlags;
        return 0;
    }

    /* get put reg. */
    if ((LSByte + Bytes) > 4)
        return HAL_ERROR_ADDR;
    i = 0;
    while ((LSByte + i) < (LSByte + Bytes)) {
        ((char*)Data_p)[i] = Str_p->_7S00_put_reg[LSByte + i];
        i++;
    }
    return HAL_OK;
}

char ASA_7s00_trm(char ASA_ID, char RegAdd, char Bytes, void* Data_p,
                  void* Str_p){
    uint8_t check = 0;
    check = ASA_7S00_put(ASA_ID, RegAdd, Bytes, Data_p, Str_p);
    return check;
}

char ASA_7s00_rec(char ASA_ID, char RegAdd, char Bytes, void* Data_p,
                  void* Str_p){
    return 0;
}

char ASA_7S00_frc(char ASA_ID, char RegAdd, char Mask, char Shift, void* Data_p,
                  void* Str_p){
    return 0;
}

char ASA_7S00_ftm(char ASA_ID, char RegAdd, char Mask, char Shift, void* Data_p,
                  void* Str_p){
    uint8_t check = 0;
    check = ASA_7S00_set(ASA_ID, RegAdd, Mask, Shift, *(char*)Data_p, Str_p);
    return check;
}
