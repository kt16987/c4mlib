/**
 * @file test_slave_twiint.c
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief 提供ASA函式庫標準中斷介面測試程式，請確認printf出來的succeed_cnt持續增加。
 */
#define USE_C4MLIB_DEBUG
#define F_CPU 11059200UL

#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/twi.h"
#include "c4mlib/time/src/hal_time.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#define TWI_SLAVE_ID 0x78
#ifndef TWI_STATUS
#define TWI_STATUS ((TWSR) & (0xF8))
#endif
//--------------------------------------------------------------------------------------------------------------------
/*Status codes for Slave transmitter mode*/
#define TWI_ST_SLA_ACK 0xA8
#define TWI_ST_SLA_Arb_lose_ACK 0xB0
#define TWI_ST_DATA_ACK 0xB8
#define TWI_ST_DATA_NACK 0xC0
#define TWI_ST_Data_last 0xC8
//--------------------------------------------------------------------------------------------------------------------
/*Status codes for Slave receiver mode*/
#define TWI_SR_SLA_ACK 0x60
#define TWI_SR_SLA_Arb_lose_ACK 0x68
#define TWI_SR_SLA_Gen_ACK 0x70
#define TWI_SR_SLA_ArbLos_Gen_ACK 0x78
#define TWI_SR_DATA_ACK 0x80
#define TWI_SR_DATA_NACK 0x88
#define TWI_SR_DATA_Gen_ACK 0x90
#define TWI_SR_DATA_Gen_NACK 0x98
#define TWI_SR_DATA_STO 0xA0
/*ACK status*/
#define USE_ACK 0x01
#define USE_NACK 0x00
//--------------------------------------------------------------------------------------------------------------------

#define USE_TWI_ISR_ID 0

TwiIntStr_t TwiInt0 = TWIINT_0_STR_INI;

typedef struct context {
    const uint8_t addr;
    uint8_t data[2];
    uint8_t cnt;
} UserContext_t;

/* User define function, when TWI interrupt, it will call. */
void USER_FUNCTION(UserContext_t* user_context);

/* Setting TWI hardware function start. */
void init_slave_twi(void);
void TWI_ACKorNAK(uint8_t ack_p);
/* Setting TWI hardware function end. */
/* Setting TIMER hardware function start. */
void init_timer(void);
/* Setting TIMER hardware function end. */

ISR(TIMER3_COMPA_vect) {
    HAL_tick();
}

int main(void) {
    C4M_STDIO_init();
    ASABUS_ID_init();
    HAL_time_init();
    init_timer();

    /***** Test the TWI interrupt*****/
    printf("======= Test TWI interrupt =======\n");
    printf("Please press salve reset first, and then press master reset.\n");

    // Define user context.
    UserContext_t TWI_context = {TWI_SLAVE_ID, {0, 0}, 0};

    /* Netting standart interrupt structure to INTERNAL_ISR. */
    TwiInt_net(&TwiInt0, USE_TWI_ISR_ID);
    /* Register the USER_FUNCTION to TWI interrupt, and TWI_context is the
     * parameters that user feed. */
    uint8_t id1 = TwiInt_reg(&TwiInt0, (Func_t)USER_FUNCTION, &TWI_context);
    TwiInt_en(&TwiInt0, id1, true);

    printf("Enable Global Interrupt\n");
    /** Initialize I2C(TWI) hardware registers as slave mode. */
    init_slave_twi();
    /** Enable interrupts */
    sei();
    uint8_t succeed_cnt = 0, ans_cnt = 0;
    while(true) {
        HAL_delay(3000);
        printf("==================\n");
        if(TWI_context.data[0] == ans_cnt) {
            if(TWI_context.data[1] == (ans_cnt+1)) {
                succeed_cnt++;
                ans_cnt +=2;
            }
        }
        DEBUG_INFO("addr:%x, data[0]:%x, data[1]:%x\n", TWI_context.addr, TWI_context.data[0], TWI_context.data[1]);
        printf("succeed_cnt : %d\n", succeed_cnt);
        printf("==================\n");
    }
}

void init_timer(void) {
    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}


void init_slave_twi(void) {
    TWAR = (TWI_SLAVE_ID<<1) | 0b00000001;
    // Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ;
    // prescaler bits = 1
    TWBR = 12;
    // enable TWI TWI_Interrupt and shack_hand for Master Start signal
    TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWEA);
}

void USER_FUNCTION(UserContext_t* user_context) { 
    uint8_t tempData = TWDR;
    DEBUG_INFO("Run user function !!!\n");
    DEBUG_INFO("TWI_STATUS:%x\n", TWI_STATUS);
    DEBUG_INFO("TWDR:%x\n", tempData);
    switch (TWI_STATUS) {
        case TWI_SR_SLA_ACK:
            TWI_ACKorNAK(USE_ACK);
        break;
        case TWI_SR_DATA_ACK:
            TWI_ACKorNAK(USE_ACK);
            user_context->data[user_context->cnt] = tempData; 
            user_context->cnt++;
            if(user_context->cnt == 2) {
                user_context->cnt = 0;
            }
        break;
        case TWI_SR_DATA_STO:
            TWI_ACKorNAK(USE_ACK);
        break;
        case TWI_ST_SLA_ACK:
        
        break;
        case TWI_ST_DATA_ACK:
        
        break;
        default:
        break;
    }

}

void TWI_ACKorNAK(uint8_t ack_p) {
    if (ack_p == USE_ACK) {
        TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA) | (1 << TWIE);
    }
    if (ack_p == USE_NACK) {
        TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWIE);
    }
}
