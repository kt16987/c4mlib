/**
 * @file test_ex7s00.c
 * @author LiYu87
 * @brief 測試7s00在外掛函式中的運作。
 * @date 2019.8.9
 */

#include "c4mlib/asa7s00/src/7s00.h"
#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/asauart/src/asauart_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/config/asamodule.cfg"

#define DELAY 10

int main() {
    C4M_DEVICE_set();
    uint8_t data[4] = {0, '-', '1', '2'};
    uint8_t res = 0;
    res = UARTM_trm(100, 2, 0, 4, data, DELAY);
    printf("res0 = %d\n", res);
}
