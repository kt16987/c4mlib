/**
 * @file adc.c
 * @author LiYu87
 * @author Deng Xiang-Guan
 * @date 2019.09.04
 * @brief
 *
 * NOTE
 * AdcInt_step is used by hardware interrupt
 * and its code is in hardware/std_isr.
 */

#include "c4mlib/hardware2/src/adc.h"

#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware2/src/adc_imp.h"
#include "c4mlib/macro/src/std_res.h"

uint8_t AdcInt_net(AdcIntStr_t *IntStr_p, uint8_t Num) {
    if (IntStr_p != NULL) {
        if (Num < ADC_HW_NUM) {
            AdcIntStrList_p[Num] = IntStr_p;
            
            IntStr_p->SetFunc_p    = AdcImp[Num].SetFunc_p;
            IntStr_p->EnFunc_p      = AdcImp[Num].EnFunc_p;
            IntStr_p->TrigFunc_p    = AdcImp[Num].TrigFunc_p;
            IntStr_p->IsDoneFunc_p  = AdcImp[Num].IsDoneFunc_p;
            IntStr_p->GetConvFunc_p = AdcImp[Num].GetConvFunc_p;

            DEBUG_INFO("Succeed to net ADC number %d\n", Num);
            return INT_OK;
        }
        else {
            // warning Num is excessive, IntStr_p can not net.
            DEBUG_INFO("Warning Num is excessive, IntStr_p can not net.\n");
            return INT_ERROR_NUM_EXCEED;
        }
    }
    else {
        return INT_ERROR_POINTER_IS_NULL;
    }
}

uint8_t AdcInt_set(AdcIntStr_t *IntStr_p) {
    if (IntStr_p != NULL) {
        return IntStr_p->SetFunc_p(IntStr_p);
    }
    else {
        return INT_ERROR_POINTER_IS_NULL;
    }
}

uint8_t AdcInt_reg(AdcIntStr_t *IntStr_p, Func_t FbFunc_p, void *FbPara_p) {
    if (IntStr_p != NULL) {
        uint8_t new_Id = IntStr_p->IntTotal;
        IntStr_p->IntFb[new_Id].enable = 0;  // Default Disable
        IntStr_p->IntFb[new_Id].func_p = FbFunc_p;
        IntStr_p->IntFb[new_Id].funcPara_p = FbPara_p;
        IntStr_p->IntTotal++;
        return new_Id;
    }
    else {
        DEBUG_INFO("Warning !!! IntStr_p is NULL\n");
        return INT_ERROR_POINTER_IS_NULL;
    }
}

void AdcInt_en(AdcIntStr_t *IntStr_p, uint8_t Fb_Id, uint8_t enable) {
    if (IntStr_p != NULL) {
        if (Fb_Id < IntStr_p->IntTotal) {
            IntStr_p->IntFb[Fb_Id].enable = enable;
        }
    }
    else {
        DEBUG_INFO("Warning !!! IntStr_p is NULL\n");
    }
}

uint8_t AdcInt_trig(AdcIntStr_t *IntStr_p) {
    if (IntStr_p != NULL) {
        return IntStr_p->TrigFunc_p(IntStr_p);
    }
    else {
        return INT_ERROR_POINTER_IS_NULL;
    }
}

uint8_t AdcInt_isDone(AdcIntStr_t *IntStr_p) {
    if (IntStr_p != NULL) {
        return IntStr_p->IsDoneFunc_p(IntStr_p);
    }
    else {
        return INT_ERROR_POINTER_IS_NULL;
    }
}

uint8_t AdcInt_en2(AdcIntStr_t *IntStr_p, uint8_t En) {
    if (IntStr_p != NULL) {
        return IntStr_p->EnFunc_p(IntStr_p, En);
    }
    else {
        return INT_ERROR_POINTER_IS_NULL;
    }
}

uint8_t AdcInt_getConv(AdcIntStr_t *IntStr_p, void *data_p) {
    if (IntStr_p != NULL) {
        return IntStr_p->GetConvFunc_p(IntStr_p, data_p);
    }
    else {
        return INT_ERROR_POINTER_IS_NULL;
    }
}
