/**
 * @file asa_twi_master.c
 * @author Yuchen
 * @date 2019-03-29
 * @brief Master TWI 通訊封包函式實現
 */

#include "c4mlib/asabus/src/exfunc.h"
#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/macro/src/bits_op.h"

#define TWI_DELAY(tick)              \
    for (int i = 0; i < tick; i++) { \
        __asm__ __volatile__("nop"); \
    }

char TWIM_trm(char mode, char SLA, char RegAdd, char Bytes, uint8_t *Data_p,
              uint16_t WaitTick) {
    uint8_t trm_check = 0; /**< Error Code Check*/
    if (mode >= 100) {
        trm_check =
            ExFunc_trm(EXFUNC_TWI, mode, SLA, RegAdd, Bytes, (uint8_t *)Data_p);
        return trm_check;
    }
    switch (mode) {
        case 1: {
            /**
             * mode 1
             * Data Sequence
             * ----------------------------------------------------------------------------------
             * |  Start  |  SLA + W |   Control Flag + LSB  |  Stop  |  Start |
             * HSB  |  Stop  |...
             * ----------------------------------------------------------------------------------
             * LSB
             *          -----------------------------------------
             * bit7     |                                       |
             * bit6     |                                       |
             * bit5     |                                       |
             * bit4     |                Data                   |
             * bit3     |                                       |
             * bit2     |                                       |
             * bit1     |                                       |
             * bit0     |                                       |
             *          -----------------------------------------
             *
             * HSB
             *          -----------------------------------------
             * bit7     |                                       |
             * bit6     |                                       |
             * bit5     |                Data                   |
             * bit4     |                                       |
             * bit3     |---------------------------------------|
             * bit2     |                                       |
             * bit1     |         Control Flag bits             |
             * bit0     |                                       |
             *          -----------------------------------------
             */

            /*Start*/
            trm_check = TWICom_Start(1);
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SLA+W*/
            //強制轉換為SLA+W
            SLA &= ~(0x01);
            trm_check = TWI_Reg_tram(SLA);
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (trm_check == TWI_MT_DATA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Data*/
            Data_p[0] |= RegAdd;
            for (int i = 0; i < Bytes; i++) {
                trm_check = TWI_Reg_tram(Data_p[i]);
                if (trm_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (trm_check == TWI_MT_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
                TWI_DELAY(WaitTick);
            }
            break;
        }
        case 2: {
            /**
             * mode 2
             * Data Sequence
             * ----------------------------------------------------------------------------------------------------------
             * |  Start  |  SLA + W |  Control Flag + HSB  |  Stop  |  Start |
             * Stop  |  Start |  SLA  | LSB | Stop  |...
             * ----------------------------------------------------------------------------------------------------------
             * HHSB
             *          -----------------------------------------
             * bit7     |                                       |
             * bit6     |         Control Flag bits             |
             * bit5     |                                       |
             * bit4     |---------------------------------------|
             * bit3     |                                       |
             * bit2     |                Data                   |
             * bit1     |                                       |
             * bit0     |                                       |
             *          -----------------------------------------
             *
             *
             * LSB
             *          -----------------------------------------
             * bit7     |                                       |
             * bit6     |                                       |
             * bit5     |                                       |
             * bit4     |                Data                   |
             * bit3     |                                       |
             * bit2     |                                       |
             * bit1     |                                       |
             * bit0     |                                       |
             *          -----------------------------------------
             */

            /*Start*/
            trm_check = TWICom_Start(1);
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SLA+W*/
            //強制轉換為SLA+W
            SLA &= ~(0x01);
            trm_check = TWI_Reg_tram(SLA);
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (trm_check == TWI_MT_DATA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Data*/
            Data_p[Bytes - 1] |= RegAdd;
            for (int i = Bytes - 1; i >= 0; i--) {
                trm_check = TWI_Reg_tram(Data_p[i]);
                if (trm_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (trm_check == TWI_MT_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
                TWI_DELAY(WaitTick);
            }
            break;
        }
        case 3: {
            /**
             * mode 3
             * Data Sequence
             * --------------------------------------------------------
             * |  Start  |  SLA + W  |  Data Low Byte First  |  Stop  |...
             * --------------------------------------------------------
             */
            /*Start*/
            trm_check = TWICom_Start(1);
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SLA+W*/
            //強制轉換為SLA+W
            SLA &= ~(0x01);
            trm_check = TWI_Reg_tram(SLA);
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (trm_check == TWI_MT_DATA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Data*/
            for (int i = 0; i < Bytes; i++) {
                trm_check = TWI_Reg_tram(Data_p[i]);
                if (trm_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (trm_check == TWI_MT_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
                TWI_DELAY(WaitTick);
            }
            break;
        }
        case 4: {
            /**
             * mode 4
             * Data Sequence
             * ---------------------------------------------------------
             * |  Start  |  SLA + W  |  Data High Byte First  |  Stop  |...
             * ---------------------------------------------------------
             */
            /*Start*/
            trm_check = TWICom_Start(1);
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SLA+W*/
            //強制轉換為SLA+W
            SLA &= ~(0x01);
            trm_check = TWI_Reg_tram(SLA);
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (trm_check == TWI_MT_DATA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Data*/
            for (int i = Bytes - 1; i >= 0; i--) {
                trm_check = TWI_Reg_tram(Data_p[i]);
                if (trm_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (trm_check == TWI_MT_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
                TWI_DELAY(WaitTick);
            }
            break;
        }
        case 5: {
            /**
             * mode 5
             * Data Sequence
             * -------------------------------------------------------------------
             * |  Start  |  SLA + W  |  RegAdd  |  Data Low Byte First  |  Stop
             * |...
             * -------------------------------------------------------------------
             */
            /*Start*/
            trm_check = TWICom_Start(1);
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SLA+W*/
            //強制轉換為SLA+W
            SLA &= ~(0x01);
            trm_check = TWI_Reg_tram(SLA);
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (trm_check == TWI_MT_DATA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*RegaAdd*/
            trm_check = TWI_Reg_tram(RegAdd);
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (trm_check == TWI_MT_DATA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Data*/
            for (int i = 0; i < Bytes; i++) {
                trm_check = TWI_Reg_tram(Data_p[i]);
                if (trm_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (trm_check == TWI_MT_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
                TWI_DELAY(WaitTick);
            }
            break;
        }
        case 6: {
            /**
             * mode 6
             * Data Sequence
             * -------------------------------------------------------------------
             * |  Start  |  SLA + W  |  Regadd  |  Data High Byte First  |  Stop
             * |...
             * -------------------------------------------------------------------
             */

            trm_check = TWICom_Start(1);
            /*Start*/
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SLA+W*/
            //強制轉換為SLA+W
            SLA &= ~0x01;
            trm_check = TWI_Reg_tram(SLA);
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (trm_check == TWI_MT_DATA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*RegAdd*/
            trm_check = TWI_Reg_tram(RegAdd);
            if (trm_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (trm_check == TWI_MT_DATA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Data*/
            for (int i = Bytes - 1; 0 <= i; i--) {
                trm_check = TWI_Reg_tram(Data_p[i]);
                if (trm_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (trm_check == TWI_MT_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
                TWI_DELAY(WaitTick);
            }
            break;
        }
    }
    /*Stop*/
    TWICom_Stop(1);
    return HAL_OK;
}

char TWIM_rec(char mode, char SLA, char RegAdd, char Bytes, uint8_t *Data_p, uint16_t WaitTick) {
    uint8_t rec_check = 0; /**< Error Code Check*/
    if (mode >= 100) {
        rec_check =
            ExFunc_rec(EXFUNC_TWI, mode, SLA, RegAdd, Bytes, (uint8_t *)Data_p);
        return rec_check;
    }
    switch (mode) {
        case 1: {
            /**
             * mode 2
             * Data Sequence
             * --------------------------------------------------------
             * |  Start  |  SLA + R  |  Data Low Byte First  |  Stop  |...
             * --------------------------------------------------------
             */
            /*Start*/
            rec_check = TWICom_Start(1);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SLA+R*/
            //強制轉換為SLA+R
            SLA |= 0x01;
            rec_check = TWI_Reg_tram(SLA);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (rec_check == TWI_MT_SLA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Data*/
            if (Bytes == 1) {
                rec_check = TWI_Reg_recNack(&Data_p[Bytes - 1]);
                if (rec_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (rec_check != TWI_MR_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
            }
            else {
                for (int i = 0; i < Bytes - 1; i++) {
                    rec_check = TWI_Reg_rec(&Data_p[i]);
                    if (rec_check == TIMEOUT_FLAG) {
                        return HAL_ERROR_TIMEOUT;
                    }
                    else if (rec_check == TWI_MR_DATA_NACK) {
                        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                    }
                    TWI_DELAY(WaitTick);
                }
                rec_check = TWI_Reg_recNack(&Data_p[Bytes - 1]);
                if (rec_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (rec_check != TWI_MR_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
            }
            /*Stop*/
            TWICom_Stop(1);
            break;
        }
        case 2: {
            /**
             * mode 1
             * Data Sequence
             * ---------------------------------------------------------
             * |  Start  |  SLA + R  |  Data High Byte First  |  Stop  |...
             * ---------------------------------------------------------
             */
            /*Start*/
            rec_check = TWICom_Start(1);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SLA+R*/
            //強制轉換為SLA+R
            SLA |= 0x01;
            rec_check = TWI_Reg_tram(SLA);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (rec_check == TWI_MT_SLA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Data*/
            if (Bytes == 1) {
                rec_check = TWI_Reg_recNack(&Data_p[Bytes - 1]);
                if (rec_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (rec_check != TWI_MR_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
            }
            else {
                for (int i = Bytes - 1; i > 0; i--) {
                    rec_check = TWI_Reg_rec(&Data_p[i]);
                    if (rec_check == TIMEOUT_FLAG) {
                        return HAL_ERROR_TIMEOUT;
                    }
                    else if (rec_check == TWI_MR_DATA_NACK) {
                        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                    }
                    TWI_DELAY(WaitTick);
                }
                rec_check = TWI_Reg_recNack(&Data_p[0]);
                if (rec_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (rec_check != TWI_MR_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
            }
            TWICom_Stop(1);
            break;
        }
        case 3: {
            /**
             * mode 2
             * Data Sequence
             * --------------------------------------------------------
             * |  Start  |  SLA + R  |  Data Low Byte First  |  Stop  |...
             * --------------------------------------------------------
             */
            /*Start*/
            rec_check = TWICom_Start(1);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SLA+R*/
            //強制轉換為SLA+R
            SLA |= 0x01;
            rec_check = TWI_Reg_tram(SLA);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (rec_check == TWI_MT_SLA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Data*/
            if (Bytes == 1) {
                rec_check = TWI_Reg_recNack(&Data_p[Bytes - 1]);
                if (rec_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (rec_check != TWI_MR_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
            }
            else {
                for (int i = 0; i < Bytes - 1; i++) {
                    rec_check = TWI_Reg_rec(&Data_p[i]);
                    if (rec_check == TIMEOUT_FLAG) {
                        return HAL_ERROR_TIMEOUT;
                    }
                    else if (rec_check == TWI_MR_DATA_NACK) {
                        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                    }
                    TWI_DELAY(WaitTick);
                }
                rec_check = TWI_Reg_recNack(&Data_p[Bytes - 1]);
                if (rec_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (rec_check != TWI_MR_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
            }
            /*Stop*/
            TWICom_Stop(1);
            break;
        }
        case 4: {
            /**
             * mode 3
             * Data Sequence
             * --------------------------------------------------------
             * |  Start  |  SLA + R  |  Data High Byte First  |  Stop  |...
             * --------------------------------------------------------
             */
            /*Start*/
            rec_check = TWICom_Start(1);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SLA+R*/
            //強制轉換為SLA+R
            SLA |= 0x01;
            rec_check = TWI_Reg_tram(SLA);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (rec_check == TWI_MT_SLA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Data*/
            if (Bytes == 1) {
                rec_check = TWI_Reg_recNack(&Data_p[Bytes - 1]);
                if (rec_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (rec_check != TWI_MR_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
            }
            else {
                for (int i = Bytes - 1; i > 0; i--) {
                    rec_check = TWI_Reg_rec(&Data_p[i]);
                    if (rec_check == TIMEOUT_FLAG) {
                        return HAL_ERROR_TIMEOUT;
                    }
                    else if (rec_check == TWI_MR_DATA_NACK) {
                        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                    }
                    TWI_DELAY(WaitTick);
                }
                rec_check = TWI_Reg_recNack(&Data_p[0]);
                if (rec_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (rec_check != TWI_MR_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
            }
            /*Stop*/
            TWICom_Stop(1);
            break;
        }
        case 5: {
            /**
             * mode 4
             * Data Sequence
             * -------------------------------------------------------------------------------------------
             * |  Start  |  SLA + W  |  RegAdd  |  Restart  |  SLA + R  |  Data
             * Low Byte First  |  Stop  |...
             * -------------------------------------------------------------------------------------------
             */
            /*Start*/
            rec_check = TWICom_Start(1);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SLA+W*/
            //強制轉換為SLA+W
            SLA &= ~0x01;
            rec_check = TWI_Reg_tram(SLA);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (rec_check == TWI_MT_SLA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*RegAdd*/
            rec_check = TWI_Reg_tram(RegAdd);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (rec_check == TWI_MT_DATA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Restart*/
            rec_check = TWICom_Start(1);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SAL+R*/
            //強制轉換為SLA+R
            SLA |= (0x01);
            rec_check = TWI_Reg_tram(SLA);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (rec_check == TWI_MR_DATA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Data*/
            if (Bytes == 1) {
                rec_check = TWI_Reg_recNack(&Data_p[Bytes - 1]);
                if (rec_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (rec_check != TWI_MR_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
            }
            else {
                for (int i = 0; i < Bytes - 1; i++) {
                    rec_check = TWI_Reg_rec(&Data_p[i]);
                    if (rec_check == TIMEOUT_FLAG) {
                        return HAL_ERROR_TIMEOUT;
                    }
                    else if (rec_check == TWI_MR_DATA_NACK) {
                        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                    }
                    TWI_DELAY(WaitTick);
                }
                rec_check = TWI_Reg_recNack(&Data_p[Bytes - 1]);
                if (rec_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (rec_check != TWI_MR_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
            }
            /*Stop*/
            TWICom_Stop(1);
            break;
        }
        case 6: {
            /**
             * mode 6
             * Data Sequence
             * -------------------------------------------------------------------------------------------
             * |  Start  |  SLA + W  |  RegAdd  |  Restart  |  SLA + R  |  Data
             * High Byte First  |  Stop  |...
             * -------------------------------------------------------------------------------------------
             */
            /*Start*/
            rec_check = TWICom_Start(1);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SLA+W*/
            //強制轉換為SLA+W
            SLA &= ~0x01;
            rec_check = TWI_Reg_tram(SLA);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (rec_check == TWI_MT_SLA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*RegAdd*/
            rec_check = TWI_Reg_tram(RegAdd);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (rec_check == TWI_MT_DATA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Restart*/
            rec_check = TWICom_Start(1);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            /*SLA+R*/
            //強制轉換為SLA+R
            SLA |= (0x01);
            rec_check = TWI_Reg_tram(SLA);
            if (rec_check == TIMEOUT_FLAG) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (rec_check == TWI_MT_SLA_NACK) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            /*Data*/
            if (Bytes == 1) {
                rec_check = TWI_Reg_recNack(&Data_p[Bytes - 1]);
                if (rec_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (rec_check != TWI_MR_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
            }
            else {
                for (int i = Bytes - 1; i > 0; i--) {
                    rec_check = TWI_Reg_rec(&Data_p[i]);
                    if (rec_check == TIMEOUT_FLAG) {
                        return HAL_ERROR_TIMEOUT;
                    }
                    else if (rec_check == TWI_MR_DATA_NACK) {
                        return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                    }
                    TWI_DELAY(WaitTick);
                }
                rec_check = TWI_Reg_recNack(&Data_p[0]);
                if (rec_check == TIMEOUT_FLAG) {
                    return HAL_ERROR_TIMEOUT;
                }
                else if (rec_check != TWI_MR_DATA_NACK) {
                    return HAL_ERROR_DEVICE_ID_NOT_FOUND;
                }
            }
            /*Stop*/
            TWICom_Stop(1);
            break;
        }
    }
    return HAL_OK;
}

char TWIM_frc(char mode, char SLA, char RegAdd, char Mask, char Shift,
              uint8_t *Data_p, uint16_t WaitTick) {
    uint8_t temp = 0;
    uint8_t frc_check = 0;
    if (mode >= 100) {
        frc_check = ExFunc_frc(EXFUNC_TWI, mode, SLA, RegAdd, Mask, Shift,
                               (uint8_t *)Data_p);
        return frc_check;
    }
    switch (mode) {
        case 3:
            frc_check = TWIM_rec(mode, SLA, RegAdd, 1, &temp, WaitTick);
            if (frc_check == HAL_ERROR_TIMEOUT) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (frc_check == HAL_ERROR_DEVICE_ID_NOT_FOUND) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            REGFGT(temp, Mask, Shift, Data_p);
            return HAL_OK;
        case 5:
            frc_check = TWIM_rec(mode, SLA, RegAdd, 1, &temp, WaitTick);
            if (frc_check == HAL_ERROR_TIMEOUT) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (frc_check == HAL_ERROR_DEVICE_ID_NOT_FOUND) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            REGFGT(temp, Mask, Shift, Data_p);
            return HAL_OK;
        default:
            return HAL_ERROR_MODE_SELECT;
    }
}

char TWIM_ftm(char mode, char SLA, char RegAdd, char Mask, char Shift,
              uint8_t *Data_p, uint16_t WaitTick) {
    uint8_t temp = 0;
    uint8_t ftm_check = 0;
    if (mode >= 100) {
        ftm_check = ExFunc_ftm(EXFUNC_TWI, mode, SLA, RegAdd, Mask, Shift,
                               (uint8_t *)Data_p);
        return ftm_check;
    }
    switch (mode) {
        case 3:
            ftm_check = TWIM_rec(mode, SLA, RegAdd, 1, &temp, WaitTick);
            if (ftm_check == HAL_ERROR_TIMEOUT) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (ftm_check == HAL_ERROR_DEVICE_ID_NOT_FOUND) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            temp = (temp & ~Mask) + ((*Data_p << Shift) & Mask);
            ftm_check = TWIM_trm(mode, SLA, RegAdd, 1, &temp, WaitTick);
            if (ftm_check == HAL_ERROR_TIMEOUT) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (ftm_check == HAL_ERROR_DEVICE_ID_NOT_FOUND) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            return HAL_OK;
        case 5:
            ftm_check = TWIM_rec(mode, SLA, RegAdd, 1, &temp, WaitTick);
            if (ftm_check == HAL_ERROR_TIMEOUT) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (ftm_check == HAL_ERROR_DEVICE_ID_NOT_FOUND) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            temp = (temp & ~Mask) + ((*Data_p << Shift) & Mask);
            ftm_check = TWIM_trm(mode, SLA, RegAdd, 1, &temp, WaitTick);
            if (ftm_check == HAL_ERROR_TIMEOUT) {
                return HAL_ERROR_TIMEOUT;
            }
            else if (ftm_check == HAL_ERROR_DEVICE_ID_NOT_FOUND) {
                return HAL_ERROR_DEVICE_ID_NOT_FOUND;
            }
            return HAL_OK;
        default:
            return HAL_ERROR_MODE_SELECT;
    }
}
