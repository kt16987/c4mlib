/**
 * @file asaspi_slave_1.c
 * @author Deng Xiang-Guan
 * @date 2019.10.02
 * @brief 提供Remote register SPI mode 1 slave端的狀態機，
 *        詳細操作請參照ASA_SPIM_S設計書。
 */
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware/src/hal_spi.h"

void ASA_SPIS1_step(void) {
    uint8_t tempData = 0;
    uint8_t cf = 0;
    switch (ASASPISerialIsrStr->sm_status) {
        case SPIS_STATE_IDLE:
            tempData = SPIS_Inst.read_byte();
            cf = tempData & 0x0F;    // 測試使用4 bits control flag
            ASASPISerialIsrStr->reg_address = cf;
            ASASPISerialIsrStr->sm_status = SPIS_STATE_DATA;
            ASASPISerialIsrStr->check_sum = 0;
            ASASPISerialIsrStr->byte_counter =
                ASASPISerialIsrStr->remo_reg[ASASPISerialIsrStr->reg_address]
                    .sz_reg -
                1;
            // 測試使用12 bits data，先儲存4bits資料
            ASASPISerialIsrStr->remo_reg[ASASPISerialIsrStr->reg_address]
                .data_p[ASASPISerialIsrStr->byte_counter] = tempData&0xF0;
            if (ASASPISerialIsrStr->byte_counter > 0) {
                ASASPISerialIsrStr->byte_counter--;
            }
            break;
        // 測試使用12 bits data，儲存剩餘8bits資料
        case SPIS_STATE_DATA:
            tempData = SPIS_Inst.read_byte();
            ASASPISerialIsrStr->remo_reg[ASASPISerialIsrStr->reg_address]
                .data_p[ASASPISerialIsrStr->byte_counter] = tempData;
            if (ASASPISerialIsrStr->byte_counter > 0) {
                ASASPISerialIsrStr->byte_counter--;
            }
            else {
                ASASPISerialIsrStr->sm_status = SPIS_STATE_IDLE;
            }
            break;
        default:
            break;
    }
    DEBUG_INFO("tempData:%x, CF_ADDR:%x, status:%d, byte_counter:%d\n",
               tempData, cf, ASASPISerialIsrStr->sm_status,
               ASASPISerialIsrStr->byte_counter);
}
