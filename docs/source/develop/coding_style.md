# Coding Style

主寫者： [LiYu87](https://gitlab.com/mickey9910326) <br>
編輯者： <br>
日期： 2019.03.14 <br>

章節為：

- 前言
- 通用規則
- C語言規則
- 命名
- 使用 Doxygen 標準註解

## 前言

為何要需要統一 coding style?

1. [Google C++ Style Guide - 背景](https://slmt.gitbooks.io/google-cpp-style-guide-zh-tw/content/1_background.html)
2. [Python coding style 1: 基本概念](https://ianliniblog.wordpress.com/2017/05/03/python-coding-style-1-%E5%9F%BA%E6%9C%AC%E6%A6%82%E5%BF%B5-linter/)

## 通用規則

1. 使用 `.editorconfig` 檔案來管理 coding style

    在專案中加入 .editorconfig 設定檔，你可以很快地開始設定專案中檔案的編輯風格
    ，包含像是縮排、tab width、EOL 字元、還有像是檔案編碼、只要編輯器認得
    editorconfig 檔案，就會在編輯或是新增檔案時自動的套用。

    [editorconfig 官網](https://editorconfig.org/)

    大部分編輯器都有模組支援 editorconfig ，請依照你使用的編輯器去安裝插件：  

    - atom 請服用模組 [editorconfig](https://atom.io/packages/editorconfig)  <br>
    - vs code 請服用模組 [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)  <br>
    - 若你的開發工具沒有editorconfig的模組，請你運用你的超強意志力來約束自己。

    以下為目前 `.editorconfig` 規定事項：  <br>

    ``` none
    root = true

    # 所有檔案
    [*]
    charset = utf-8              # 使用utf-8作為檔案編碼
    end_of_line = lf             # 換行模式
    insert_final_newline = true  # 檔案結尾要換行

    # 副檔名為c之檔案
    [*.c]
    indent_style = space  # 縮排方式為空白
    indent_size = 4       # 縮排一次為4個空白

    # 副檔名為h之檔案
    [*.h]
    indent_style = space  # 縮排方式為空白
    indent_size = 4       # 縮排一次為4個空白

    # 檔名為Makefile之檔案
    [Makefile]
    indent_style = tab  # 縮排方式為tab(\t)

    # 副檔名為mk之檔案
    [*.mk]
    indent_style = tab  # 縮排方式為tab(\t)
    ```

2. 撰寫風格需照此文件之規定，若有未規定處請遵循google coding style

    在撰寫程式時，請依照本文件的規定去執行，但細節眾多，可能有些程式細節沒有規範到，若有未規範之部分，請依照google的程式風格去執行。  
    [Google C++ Style Guide](https://zh-google-styleguide.readthedocs.io/en/latest/google-cpp-styleguide/)

3. 一行的文字量最好不要超過80個半型字，提升可讀性

    可參考[Google C++ 风格指南 9.1. 行长度](https://zh-google-styleguide.readthedocs.io/en/latest/google-cpp-styleguide/formatting/#id2)

4. 註解盡量使用中文

    大部分的情況註解請用中文，因維護人員都使用中文，不能假設他人都看的懂英文，
    就算看得懂，花費時間時間也會比中文來的高。

    不過幾種情況下可以使用英文：

    1. 說明縮寫

        ``` c
        uint8_t fs; // formate string
        ```

    2. 簡短的英文即可表達 (10個英文單字內)

        ``` c
        // Reset timeout
        // 逾時時間重置
        ```

5. 使用通用註記

    有一些標記是常用，且許多編輯器都會自動高亮提示。
    像：TODO、FIXME、NOTE

    TODO 標記未完成之事項 <br>
    ex: `// TODO: Handle ID not exist exception here`

    FIXME 標記問題需要處理的地方 <br>
    ex: `// FIXME: 效率太低，需要降低複雜度` <br>
    通常FIXME不該在程式碼出現中，除非當前真的沒有時間處理、或無法處理，才能使用。 <br>
    在分支要merge時，若有FIXME出現是一件非常糟糕的事情，而且這代表了此分支會被reject。

    NOTE 標記重要的先備知識，或需要注意的地方 <br>
    ex: `# NOTE : 編譯前請先使用 make clean 清除所有目標檔`

6. 維護你的註解

    當你的程式碼更新時，有些註解可能會改變，請務必要更新註解，不然就直接刪除它。
    註解與程式碼不同時造成的成本遠比沒有註解高，所以當你寫出註解時，請對自己的註
    解負責。像完成TODO或FIXME的註解時，請務必刪除它。

7. 禁止在行尾添加無意義空白

## C 語言規則

1. 標頭檔使用 Define Guard

    每個標頭檔一定要有#define Guard，避免重複引入。
    macro應為 `<PROJECT>_<MODULE>_<FILE>_H`

    Ex：c4mlib/hmi/src/hmi.h  

    ``` c
    #ifndef C4MLIB_HMI_HMI_H
    #define C4MLIB_HMI_HMI_H
    ...
    #endif  // C4MLIB_HMI_HMI_H
    ```

    define Guard 詳細說明可以參照[Google C++ 风格指南 1.2. #define Guard](https://zh-google-styleguide.readthedocs.io/en/latest/google-cpp-styleguide/headers/#define)

2. 標頭檔對外公開區段請加上特定註解  

    因為開發c4mlib的目的是方便他人使用，釋出時只會有一個header。
    所以在專案建置時，python的腳本會自動讀取以特定註解標記的區段，
    並整合到`c4mlib.h`中，請照下面的範例來標記要開放的header片段。

    Ex：  

    ``` c
    /* Public Section Start */
    uint8_t myfun();
    /* Public Section End */
    ```

3. #include 引用規則  

    1. 路徑全部使用正斜線`/`。

    2. 若要引用其他模組的標頭皆以c4mlib作為開頭。

        在模組預設的makefile中已經把專案跟目錄加入INCLUDE PATH，所以可以直接以c4mlib作為起始資料夾引用標頭。
        提升程式的可讀性、找標頭時也方便。

        Ex：  

        ``` c
        #include "c4mlib/hmi/src/hmi.h"
        ```

    3. 引用complier內建lib時，使用`<>`。

        在引用complier內建lib時，一律使用`<>`，禁止使用`""`。<br>
        Ex：  

        ``` c  
        #include <stdio.h>
        #include <avr/io.h>
        ```

    4. 保持最低限度的引用

        並外請勿引用沒使用的標頭檔，造成困擾。

    5. 引用的順序

        使用標準順序以提高可讀性並避免隱藏的依賴關係。順序請照以下排列，並在每個種類間加上一行空白行。

        1. 關聯的標頭檔  
        2. C library、avrlibc
        3. 其餘模組標頭引用  
        4. 設定檔cfg引入  

        Ex：  

        ``` c  
        #include "asahmi.h"

        #include <stdio.h>
        #include <stdint.h>
        #include <avr/io.h>

        #include "c4mlib/asabus/src/asabus.h"

        #include "c4mlib/config/interrupt.cfg"
        ```

4. 使用static

    若一個全域變數只在此檔案使用，請加上static，讓他無法在其他c file中作用。<br>
    詳細說明可以參照[Google C++ 风格指南 2.3. 非成员函数、静态成员函数和全局函数](https://zh-google-styleguide.readthedocs.io/en/latest/google-cpp-styleguide/scoping/#id4)

5. 保持函式簡短

    請盡量把函式切割，簡短化。

    較長的函式有時是合理的，因此並不硬性限制函式的長度。如果函式超過40行，可以思索一下能不能在不影響程序結構的前提下對其進行分割。

    即使一個長函式現在工作的非常好，一旦有人對其修改，有可能出現新的問題，甚至導致難以發現的bug。使用數量簡短，以便於他人閱讀和修改代碼。

    在處理代碼時，你可能會發現複雜的長函式。不要害怕修改現有代碼：如果證實這些代碼使用/調試起來很困難，或者你只需​​要使用其中的一小段代碼，考慮將其分割為更加簡短並易於管理的若干函式。

6. 少用數字，利用 MACRO 把數字變成有意義的文字。

## 命名

### 變數

1. 全域變數大寫開頭
2. 區域變數小寫開頭
3. 指標變數在結尾加上`_p`註記  <br>
    若一個變數是指標時，請在結尾加上 _p，作為標記。

### 函式參數

1. 開頭小寫  
2. 使用駝峰式命名  
   Ex: `regAddr`

### MACRO

1. 一律大寫
2. 使用`_`串接不同單字

### 結構

1. 結構需要以typedef宣告

    ex:

    ``` c
    typedef struct {
        const uint8_t EXTI_UID;                           // 紀錄本結構歸屬UID，用於init()函式判斷鏈接目標
        uint8_t total;                                    // 紀錄已有多少逾時中斷已註冊
        volatile IsrFunc_t task[MAX_EXTINT_NUM];      // 紀錄所有已註冊的外部中斷
    } ExtInt_t;
    ```

2. 型態名需要以`_t`結尾，並為駝峰式命名
3. 常數成員名稱為全大寫，並以`_`切割單字
4. 非常數成員名稱以小寫開頭，並為駝峰式命名
  
## 格式

1. 大括號不換行

    ex:

    ``` c
    if (xxx) {

    }
    ```

2. case要加上大括號

    ex:

    ``` c
    case 1: {
      xxx;
      break;
    }
    ```

3. 太長的 function 宣告或呼叫可以換行

    ex:

    ``` c
    func(
      z_xxxxxxxxxxxxxxxx->aasd,
      a_xxxxxxxxxxxxxxxx->aasd,
      d_xxxxxxxxxxxxxxxx->aasd,
    );
    ```

    ex:

    ``` c
    void func(
      int z_xxxxxxxxxxxxxxxx,
      float a_xxxxxxxxxxxxxxxx,
      ddd_t d_xxxxxxxxxxxxxxxx,
    ) {
      xxxx;
    }
    ```

## 使用 Doxygen 標準註解

  本專案使用了 Doxygen + Sphinx 去建置文檔，所以需在原始碼中加入 Doxygen
  格式的註解，以便自動產生文檔。

  Doxygen 是一套程式文件的產生器，會抓取程式的內容、註解來自動產生程式說明文件的系統。 <br>
  [Doxygen 官網](http://www.doxygen.nl/) <br>
  [Doxygen 維基百科介紹](https://zh.wikipedia.org/wiki/Doxygen) <br>

  以下是幫助產生 Doxygen 格式註解的模組： <br>
  atom 請服用模組 [docblockr](https://atom.io/packages/docblockr)  <br>
  vs code 請服用模組 [Doxygen Documentation Generator](https://marketplace.visualstudio.com/items?itemName=cschlosser.doxdocgen)  <br>
  若你的開發工具沒有Doxygen註解用的模組，請你運用你的超強意志力加上執行力。

  以下是一些常用規則：

### 檔案開頭格式

  ``` c
  /**
   * @file hmi.h
   * @author LiYu
   * @date 2019.01.28
   * @brief 放置HMI函式原型及Macro
   */
  ```

### 函式原型格式

  函式原型一定要在某一group中，這樣再生成文檔時才可以自動分類。

  defgroup 可以定義群組，並只用宣告一次。<br>

  ``` c
  /**
  * @defgroup hmi
  */
  ```

  我們在註解函式原型時，就可以使用ingroup來標記這個函示是在哪個群組的。<br>

  ``` c
  /**
  * @ingroup hmi
  *
  * @brief 發送矩陣資料Data_p給HMI。
  *
  * @param  Bytes  矩陣資料位元組大小
  * @param  Type   資料型態編號，詳見HMI資料型態編號
  * @param  Data_p 矩陣資料起始記憶體
  * @return        0：成功無誤。<br>
  *            	  1：資料大小錯誤。<br>
  *            	  2：資料型態代碼錯誤，請檢察Type 參數是否正確。<br>
  *            	  3：同步失敗，請檢察HMI方是否也是使用同步版函式。<br>
  *            	  4：通訊錯誤，請檢查雙方程式流程是否對應。
  */
  char HMI_put_array(int Bytes, char Type, void *Data_p);
  ```

### 其餘請參照

- [Doxygen usage example (for C)](http://fnch.users.sourceforge.net/doxygen_c.html)
- [Doxygen 官網 Comment blocks for C-like languages](http://www.doxygen.nl/manual/docblocks.html#cppblock)
