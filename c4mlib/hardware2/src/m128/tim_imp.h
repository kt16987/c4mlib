#ifndef C4MLIB_HARDEARE2_TIM_IMP_H
#define C4MLIB_HARDEARE2_TIM_IMP_H

#include "c4mlib/hardware2/src/tim.h"

/* Public Section Start */
#define TIM_HW_NUM 4
extern TimIntStr_t TimImp[TIM_HW_NUM];
extern TimIntStr_t *TimIntStrList_p[TIM_HW_NUM];
/* Public Section End */

#endif  // C4MLIB_HARDEARE2_TIM_IMP_H
