/**
 * @file uart.c
 * @author LiYu87
 * @brief ASA M128 之ASABUS IO 之硬體初始化。
 * @date 2019.7.15
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

#define DEFAULTUARTBAUD 38400

void ASABUS_UART_init(void) {
    uint16_t baud = F_CPU / 16 / DEFAULTUARTBAUD - 1;

    UBRR1H = (uint8_t)(baud >> 8);
    UBRR1L = (uint8_t)baud;

    UCSR1B |= (1 << RXEN1) | (1 << TXEN1);
    UCSR1C |= (3 << UCSZ10);
}

void ASABUS_UART_transmit(char data) {
    while (!(UCSR1A & (1 << UDRE1)))
        ;
    UDR1 = data;
}

char ASABUS_UART_receive(void) {
    while (!(UCSR1A & (1 << RXC1)))
        ;
    return UDR1;
}
