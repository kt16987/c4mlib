/**
 * @file test_master_remorw_trn_mode6.c
 * @author TODO
 * @date TODO
 * @brief TODO
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_master.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/time/src/hal_time.h"
#include "c4mlib/hardware/src/spi.h"
int data=200;
int dataput=5;
int prevent=0;

#define SPI_MODE 6
#define DELAY 10

int main()
{
    C4M_DEVICE_set();
    SPI_fpt(200,0x03,0,3);//除頻值設定為f/64
    SPI_fpt(201,0x01,0,1);//除頻值設定為f/64
    ASA_SPIM_trm(SPI_MODE, 1, 3, 2, &data, DELAY);
    printf("get\n");
    _delay_ms(3000);
    while(1)
    {
        if(prevent==0)
        {
            //ASA_STP00_put(1,Lsbyteput,Byte,&putdata1);
            printf("move\n");
            ASA_SPIM_trm(SPI_MODE, 1, 3, 2, &dataput, DELAY);
            prevent=1;
        }
        else
        {
            //ASA_STP00_put(1,Lsbyteput,Byte,&putdata1);
            ASA_SPIM_trm(SPI_MODE, 1, 3, 2, &dataput, DELAY);
            prevent=0;
        }
        
        _delay_ms(200);
    
    }
}


