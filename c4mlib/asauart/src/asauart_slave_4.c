/**
 * @file asauart_slave_4.c
 * @author Ye cheng-Wei
 * @author Wu Cheng Han
 * @date 2019.10.07
 * @brief 實現 ASA Slave UART Mode4 通運封包函式
 *
 * 提供 UART Slave端 Mode4 通訊封包函式 ASA_UARTS4_rx_step、ASA_UARTS4_tx_step，需先將ASA_UARTS4_rx_step
 * 註冊進Uart Rx中斷函式、ASA_UARTS4_tx_step 註冊進Uart Tx中斷函式。
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/macro/src/std_res.h"
#include "c4mlib/time/src/timeout.h"

#include "asauart_slave.h"

static uint8_t timeout_IsrID;
static uint8_t timeout_flag = 0;
static uint8_t byte_count_flag = 0;

void ASA_UARTS4_rx_step() {
    if (ASAUARTSerialIsrStr == NULL)
        return;  // If SerialIsrStr_init is not call first, There will return
    ASAUARTSerialIsrStr->reg_address = 2;

    DEBUG_INFO("ASA_UARTS4_rx_step call [Timeout:%u]\n", timeout_flag);

    uint8_t data_In;
    if (ASAUARTSerialIsrStr->sm_status == UARTS_SM_HEADER) {
        ASAUARTSerialIsrStr->sm_status =
            UARTS_SM_DATA;  //因狀態機沒Header狀態，所以直接引至Data狀態
    }

    switch (ASAUARTSerialIsrStr->sm_status) {
        case UARTS_SM_DATA:
            if (timeout_flag) {  // 逾時發生 // ? "逾時發生，狀態機需要做什麼嗎"
                ASAUARTSerialIsrStr->result_message =
                    HAL_ERROR_TIMEOUT;  // 回傳Timeout error 錯誤訊息
                Timeout_ctl(timeout_IsrID, 0);  // 關閉逾時中斷
                break;
            }
            if (byte_count_flag == 0) {
                ASAUARTSerialIsrStr->byte_counter =
                    ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .sz_reg;
                byte_count_flag = 1;
            }
            UARTS_Inst.read_byte(&data_In,0);  // 讀取 1 byte 資料
            DEBUG_INFO("[UARTS_SM_DATA]\tread <%02X>\n", data_In);
            DEBUG_INFO(
                "byte_counter: <%u>, sz_reg: <%u>\n",
                ASAUARTSerialIsrStr->byte_counter,
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .sz_reg);
            // 將讀取到的 1 byte 資料 存入 temp
            ASAUARTSerialIsrStr->temp[ASAUARTSerialIsrStr->byte_counter - 1] =
                data_In;
            ASAUARTSerialIsrStr->byte_counter--;

            if (ASAUARTSerialIsrStr->byte_counter == 0) {
                DEBUG_INFO("Process write mode\n");
                DEBUG_INFO("ASAUARTSerialIsrStr address:%x\n",
                           ASAUARTSerialIsrStr);
                // Move temporary data in buffer to target register memory
                for (int i = 0;
                     i < ASAUARTSerialIsrStr
                             ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                             .sz_reg;
                     i++) {
                    ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .data_p[i] = ASAUARTSerialIsrStr->temp[i];
                }
                // Process Modify event Callback
                if (ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .func_p != NULL)
                    ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .func_p(ASAUARTSerialIsrStr
                                    ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                                    .funcPara_p);

                DEBUG_INFO("Process write register Done\n");

                ASAUARTSerialIsrStr->byte_counter =
                    ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .sz_reg -
                    1;
                byte_count_flag = 0;
                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);

                /* ---------------- 呼叫 Tx 傳送資料 -------------------*/
                ASA_UARTS4_tx_step();
            }

            break;
    }

    // Reset the timeout ISR
    TimerCntStr_inst.timeoutISR_inst[timeout_IsrID].counter =
        TimerCntStr_inst.timeoutISR_inst[timeout_IsrID].time_limit;
    timeout_flag = 0;  // timeout 旗標放下
}

void ASA_UARTS4_tx_step() {
    DEBUG_INFO("ASA_UARTS4_tx_step call [Timeout:%u]\n", timeout_flag);
    ASAUARTSerialIsrStr->reg_address = 3;
    DEBUG_INFO(
        "byte_counter: <%u>, sz_reg: <%u>\n", ASAUARTSerialIsrStr->byte_counter,
        ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address].sz_reg);
    if (byte_count_flag == 0) {
        ASAUARTSerialIsrStr->byte_counter =
            ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                .sz_reg;
        byte_count_flag = 1;
    }
    _delay_ms(45);  // 讓tx delay，不然太快會使rec漏接資料

    if (timeout_flag) {  // 逾時發生 // ? "逾時發生，狀態機需要做什麼嗎"
        ASAUARTSerialIsrStr->result_message =
            HAL_ERROR_TIMEOUT;          // 回傳Timeout error 錯誤訊息
        Timeout_ctl(timeout_IsrID, 0);  // 關閉逾時中斷
    }
    /**** Write the Data to Master ****/

    if (ASAUARTSerialIsrStr->byte_counter == 0) {
        ASAUARTSerialIsrStr->byte_counter =
            ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                .sz_reg;
        byte_count_flag = 0;
        Timeout_ctl(timeout_IsrID, 0);  // 關閉逾時中斷
    }
    else {
        UARTS_Inst.write_byte(
            ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                .data_p[ASAUARTSerialIsrStr->byte_counter - 1],0);

        ASAUARTSerialIsrStr->byte_counter--;
    }
}
