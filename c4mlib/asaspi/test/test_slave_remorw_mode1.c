/**
 * @file test_slave_remorw_mode1.c
 * @author Deng Xiang-Guan
 * @date 2019.10.04
 * @brief 測試ASA_SPIM_trm和ASA_SPIM_rec mode 1函式。
 * 
 * 需搭配test_master_mem_trm_mode5.c，測試SPI Master(主)記憶傳輸資料，由低到高
 * ，接收來自Master端的資料，測試方式如下條列表示：
 *   1. 是否發生SPI中斷，條件為SPI Master端 chip select為Low觸發。
 *   2. 發生中斷後，檢查Master傳送的command、memery address，如果都正確，接收
 *      Master端的資料。
 *   3. 檢查Master端的資料是否和測試資料一致。
 *   4. 如果資料正確，成功計數器會增加，且於終端機顯示出成功次數。  
 *   5. 回流程1。
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware/src/eeprom.h"
#include "c4mlib/hardware/src/hal_spi.h"
#include "c4mlib/time/src/hal_time.h"

void init_timer(void);

uint16_t test_ans1 = 0x87;

int main() {
    // Setup
    C4M_STDIO_init();
    printf("Start slave mode 1\n");

    // Initailize Serial SPI remote register type
    SerialIsr_t SPIIsrStr = SERIAL_ISR_STR_SPI_INI;
    SerialIsr_net(&SPIIsrStr, ASA_SPIS1_step);

    uint8_t reg_1[2] = {0, 0};

    uint8_t reg_1_Id = RemoRW_reg(&SPIIsrStr, reg_1, 2);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_1_Id,
           SPIIsrStr.remo_reg[reg_1_Id].sz_reg);

    char er_flag = false;
    char ok_flag = 0;
    printf("Start test slave remote read write register!!!\n");
    _delay_ms(3000);
    while (true) {
        // Show the register data

        if(((reg_1[0]<<4) | (reg_1[1]>>4)) == test_ans1) {
            test_ans1++;
            ok_flag++;
            er_flag = false;
        }
        else {
            er_flag = true;
        }

        printf("\n");
        printf("\n=================\n");
        if (er_flag) {
            printf("Test slave mode 1 remo-reg fail !!!\n");
            while (1)
                ;
        }
        else {
            printf("Succeed --> %d\n", ok_flag);
        }
        if (ok_flag >= 5) {
            printf("Test slave mode 1 remo-reg succeed !!!\n");
            break;
        }
        _delay_ms(3000);
    }
}
