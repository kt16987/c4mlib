/**
 * @file test_slave_remorw_frc_mode3.c
 * @author Deng Xiang-Guan
 * @date 2019.10.04
 * @brief 測試ASA_SPIM_frc mode3函式。
 * 
 * 需搭配test_master_remorw_frc_mode3.c，測試SPI Master(主)mode 3旗標接收資料
 * ，不具檢查機制，測試方式如下條列表示：
 *   1. Remote Register的結構宣告，並執行初始化結構的鏈結。
 *   2. 註冊需要的暫存器，需給予空間，本次測試註冊兩個暫存器，大小分別為1、1。
 *   3. 註冊後回傳的每個暫存器的編號，此編號為Master端讀寫暫存器的位置。
 *   4. 是否發生SPI中斷，條件為SPI Master端 chip select為Low觸發。
 *   5. 中斷時將發送Master端欲讀取的資料。
 *   6. 更新Slave端暫存器的資料，回到流程4。
 */

#include "c4mlib/asabus/src/asabus.h"
#include "c4mlib/asabus/src/pin_def.h"
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asaspi/src/asaspi_slave.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/time/src/hal_time.h"
#include "c4mlib/hardware/src/eeprom.h"
#include "c4mlib/hardware/src/hal_spi.h"

int main() {
    // Setup
    C4M_STDIO_init();
    printf("Start frc slave mode 3\n");

    // Initailize Serial SPI remote register type
    SerialIsr_t SPIIsrStr = SERIAL_ISR_STR_SPI_INI;
    SerialIsr_net(&SPIIsrStr, ASA_SPIS3_step);

    uint8_t reg_1[1] = {0};
    uint8_t reg_2[1] = {0};

    uint8_t reg_1_Id = RemoRW_reg(&SPIIsrStr, reg_1, 1);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_1_Id,
           SPIIsrStr.remo_reg[reg_1_Id].sz_reg);

    uint8_t reg_2_Id = RemoRW_reg(&SPIIsrStr, reg_2, 1);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_2_Id,
           SPIIsrStr.remo_reg[reg_2_Id].sz_reg);

    SPIIsrStr.remo_reg[reg_1_Id].data_p[0] = 87;
    SPIIsrStr.remo_reg[reg_2_Id].data_p[0] = 255;

    ASASPISerialIsrStr->rw_mode = 1;
    while (true) {
        // Show the register data
    }
}
