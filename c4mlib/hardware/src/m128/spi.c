/**
 * @file spi.c
 * @author LiYu87
 * @date 2019.07.15
 * @brief spi 暫存器操作相關函式實作。
 */

#include "c4mlib/hardware/src/spi.h"

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

char SPI_fpt(char LSByte, char Mask, char Shift, char Data) {
    switch (LSByte) {
        case 200:
            REGFPT(SPCR, Mask, Shift, Data);
            break;
        case 201:
            REGFPT(SPSR, Mask, Shift, Data);
            break;
        case 202:
            REGFPT(DDRB, Mask, Shift, Data);
            break;
        default:
            return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}

char SPI_fgt(char LSByte, char Mask, char Shift, void* Data_p) {
    switch (LSByte) {
        case 200:
            REGFGT(SPCR, Mask, Shift, Data_p);
            break;
        case 201:
            REGFGT(SPSR, Mask, Shift, Data_p);
            break;
        case 202:
            REGFGT(DDRB, Mask, Shift, Data_p);
            break;
        default:
            return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}

char SPI_put(char LSByte, char Bytes, void* Data_p) {
    switch (LSByte) {
        case 0: {
            if (Bytes == 1) {
                SPDR = *((uint8_t*)Data_p);
            }
            else {
                return RES_ERROR_BYTES;
            }
            break;
        }
        default: { return RES_ERROR_LSBYTE; }
    }
    return RES_OK;
}

char SPI_get(char LSByte, char Bytes, void* Data_p) {
    switch (LSByte) {
        case 0: {
            if (Bytes == 1) {
                *((uint8_t*)Data_p) = SPDR;
            }
            else {
                return RES_ERROR_BYTES;
            }
            break;
        }
        default: { return RES_ERROR_LSBYTE; }
    }
    return RES_OK;
}
