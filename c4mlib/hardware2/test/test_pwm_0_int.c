/**
 * @file test_pwm_0_int.c
 * @author LiYu87
 * @date 2019.09.05
 * @brief
 *
 * 確認有輸出 enter inerttupt!! ，以保證有進入 PWM0 中斷
 * 測試裝置：ASA_M128_V2
 * 測試硬體：TIMER 0
 */

#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/pwm.h"

#include "c4mlib/config/pwm.cfg"

void int_func(void *);

int main(void) {
    C4M_DEVICE_set();

    printf("PWM init test ------------------------------------------------\n");
    sei();

    PwmIntStr_t pwm0 = PWM0_INIT;

    PwmInt_net(&pwm0, 0);

    PwmInt_reg(&pwm0, int_func, 0);
    PwmInt_en(&pwm0, 0, 1);

    PwmInt_set(&pwm0);

    while (1) {
        asm("nop");
    }

    return 0;
}

void int_func(void *p) {
    printf("enter inerttupt!! \n");
}
