/**
 * @file test_rtfio_adcint.c
 * @author Yi-Mou
 * @brief rtfio實作
 * @date 2019-08-19
 *
 * timint呼叫RralTimeFlagOut使adc運作
 * adc處理完畢呼叫RealTimePort取值
 */

#include "c4mlib/device/src/device.h"
#include <avr/io.h>

#include "c4mlib/rtpio/src/rtpio.h"
#include "c4mlib/hardware2/src/adc.h"
#include "c4mlib/hardware2/src/tim.h"
#include "c4mlib/config/adc.cfg"
#include "c4mlib/config/tim.cfg"

#define adcstart 1

int main(){

    C4M_DEVICE_set();

    TimIntStr_t TimInt0_str = TIM0_INIT;
    AdcIntStr_t AdcInt0_str = ADC0_STR_INI;
    RealTimeFlagStr_t AdcStartFlag_str={0};
    RealTimePortStr_t AdcPort_str={0};

    TimInt_net(&TimInt0_str,0);
    AdcInt_net(&AdcInt0_str,0);
    AdcInt0_str.AdcSet.IntEn = 1;

    RealTimeFlag_net(&AdcStartFlag_str,(uint8_t *)_SFR_ADDR(ADCSRA),1<<(ADSC),ADSC);
    RealTimePort_net(&AdcPort_str,(uint8_t *)_SFR_ADDR(ADCL),2);

    AdcStartFlag_str.Fb_Id = TimInt_reg (&TimInt0_str, (Func_t)RealTimeFlagOut_step, &AdcStartFlag_str);
    TimInt_en(&TimInt0_str,AdcStartFlag_str.Fb_Id,true);

    AdcPort_str.Fb_Id = AdcInt_reg (&AdcInt0_str, (Func_t)RealTimePortIn_step, &AdcPort_str);
    AdcInt_en(&AdcInt0_str,AdcPort_str.Fb_Id, true);

    TimInt_set(&TimInt0_str);
    OCR0 = 53;
    AdcInt_set(&AdcInt0_str);

    sei();

    uint16_t data=0;
    while(1){
        if(AdcStartFlag_str.TrigCount==1){
            AdcStartFlag_str.FlagsValue = 1;
            AdcStartFlag_str.TrigCount=0;
        }
        else if(AdcStartFlag_str.TrigCount>1){
            printf("too fast\n");
        }

        if(AdcPort_str.TrigCount==1){
            data = *(unsigned int*)AdcPort_str.Buff;
            AdcPort_str.TrigCount=0;
        }
        else if (AdcPort_str.TrigCount>1){
            printf("too fast2\n");
        }
        printf("%u\n",data);
    }
}
