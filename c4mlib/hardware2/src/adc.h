/**
 * @file adc.h
 * @author LiYu87
 * @author Deng Xiang-Guan
 * @date 2019.09.04
 * @brief
 *
 * 提供ASA函式庫標準中斷介面，將原生硬體Interrupt呼叫函式占用，並提供登陸函式介面。
 */

#ifndef C4MLIB_HARDWARE2_ADC_H
#define C4MLIB_HARDWARE2_ADC_H

#include "c4mlib/interrupt/src/isr_func.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

// Include Configure file
#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct __adc_set_str {
    uint8_t AdcConvEn;      ///< 是否啟用ADC轉換
    uint8_t Channel;        ///< ADC取樣通道
    uint8_t ClockPrescaler; ///< 除頻值
    uint8_t Resolution;     ///< 解析度
    uint8_t DataAlign;      ///< 資料對其
    uint8_t Reference;      ///< 參考電壓
    uint8_t ConvMode;       ///< 轉換模式
    uint8_t IntEn;          ///< 中斷是否啟用
} AdcSetStr_t;

typedef struct __adc_int_str {
    AdcSetStr_t AdcSet;

    uint8_t (*SetFunc_p)(struct __adc_int_str *);
    uint8_t (*EnFunc_p)(struct __adc_int_str *, uint8_t);
    uint8_t (*TrigFunc_p)(struct __adc_int_str *);
    uint8_t (*IsDoneFunc_p)(struct __adc_int_str *);
    uint8_t (*GetConvFunc_p)(struct __adc_int_str *, void *);

    uint8_t IntTotal;                                   ///< 紀錄已有多少中斷已註冊
    volatile FuncBlockStr_t IntFb[MAX_ADCINT_FUNCNUM];  ///< 紀錄所有已註冊的中斷函式
} AdcIntStr_t;

uint8_t AdcInt_net(AdcIntStr_t *IntStr_p, uint8_t Num);

uint8_t AdcInt_set(AdcIntStr_t *IntStr_p);

uint8_t AdcInt_reg(AdcIntStr_t *IntStr_p, Func_t FbFunc_p, void *FbPara_p);

void AdcInt_en(AdcIntStr_t *IntStr_p, uint8_t Fb_Id, uint8_t enable);

uint8_t AdcInt_trig(AdcIntStr_t *IntStr_p);
uint8_t AdcInt_isDone(AdcIntStr_t *IntStr_p);
uint8_t AdcInt_en2(AdcIntStr_t *IntStr_p, uint8_t En);
uint8_t AdcInt_getConv(AdcIntStr_t *IntStr_p, void *data_p);

void AdcInt_step(AdcIntStr_t *IntStr_p);
/* Public Section End */

/* Public Section Start */
/*----- Adc Sets Macros -----------------------------------------------------*/
#define ADC_CHANNEL_0 0
#define ADC_CHANNEL_1 1
#define ADC_CHANNEL_2 2
#define ADC_CHANNEL_3 3
#define ADC_CHANNEL_4 4
#define ADC_CHANNEL_5 5
#define ADC_CHANNEL_6 6
#define ADC_CHANNEL_7 7
#define ADC_CHANNEL_1230MV 30
#define ADC_CHANNEL_GND 31

#define ADC_SINGLE_END_0_X1 0
#define ADC_SINGLE_END_1_X1 1
#define ADC_SINGLE_END_2_X1 2
#define ADC_SINGLE_END_3_X1 3
#define ADC_SINGLE_END_4_X1 4
#define ADC_SINGLE_END_5_X1 5
#define ADC_SINGLE_END_6_X1 6
#define ADC_SINGLE_END_7_X1 7

#define ADC_DIFF_00_X10 8
#define ADC_DIFF_10_X10 9
#define ADC_DIFF_00_X200 10
#define ADC_DIFF_10_X200 11
#define ADC_DIFF_22_X10 12
#define ADC_DIFF_32_X10 13
#define ADC_DIFF_22_X200 14
#define ADC_DIFF_32_X200 15

#define ADC_DIFF_01_X1 16
#define ADC_DIFF_11_X1 17
#define ADC_DIFF_21_X1 18
#define ADC_DIFF_31_X1 19
#define ADC_DIFF_41_X1 20
#define ADC_DIFF_51_X1 21
#define ADC_DIFF_61_X1 22
#define ADC_DIFF_71_X1 23

#define ADC_DIFF_02_X1 24
#define ADC_DIFF_12_X1 25
#define ADC_DIFF_22_X1 26
#define ADC_DIFF_32_X1 27
#define ADC_DIFF_42_X1 28
#define ADC_DIFF_52_X1 29

#define ADC_CALIB_GAIN 30
#define ADC_CALIB_GND  31

#define ADC_CLOCKPRESCALER_2 2
#define ADC_CLOCKPRESCALER_4 4
#define ADC_CLOCKPRESCALER_6 6
#define ADC_CLOCKPRESCALER_8 8
#define ADC_CLOCKPRESCALER_16 16
#define ADC_CLOCKPRESCALER_32 32
#define ADC_CLOCKPRESCALER_64 64
#define ADC_CLOCKPRESCALER_128 128

#define ADC_RESOLUTION_8  8
#define ADC_RESOLUTION_10 10

#define ADC_RESOLUTION_8  8
#define ADC_RESOLUTION_10 10

#define ADC_REF_AREF 0
#define ADC_REF_AVCC 1
#define ADC_REF_2560MV 3

#define ADC_DATAALIGN_RIGHT 0
#define ADC_DATAALIGN_LEFT 1

#define ADC_CONVMODE_SUCCESIVELY 1
#define ADC_CONVMODE_TRIGERED 0
/*---------------------------------------------------------------------------*/
/* Public Section End */


#endif  // C4MLIB_HARDWARE2_ADC_H
