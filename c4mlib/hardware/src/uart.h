/**
 * @file uart.h
 * @author LiYu87
 * @date 2019.04.06
 * @brief 宣告 uart 暫存器操作函式原型。
 */

#ifndef C4MLIB_HARDWARE_UART_H
#define C4MLIB_HARDWARE_UART_H

/**
 * @defgroup hw_uart_func hardware uart functions
 * @ingroup hw_uart
 */

/* Public Section Start */
/**
 * @brief UART flag put函式
 *
 * @ingroup hw_uart_func
 * @param LSByte 暫存器編號。
 * @param Mask   遮罩。
 * @param Shift  向左位移。
 * @param Data   寫入資料。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 LSByte 錯誤。
 *   - 4：參數 Mask 錯誤。
 *   - 5：參數 Shift 超出範圍。
 */
char UART_fpt(char LSByte, char Mask, char Shift, char Data);

/**
 * @brief ASA UART flag get函式
 *
 * @ingroup hw_uart_func
 * @param LSByte 暫存器編號。
 * @param Mask   遮罩。
 * @param Shift  向右位移。
 * @param Data_p 資料指標。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 LSByte 錯誤。
 *   - 4：參數 Mask 錯誤。
 *   - 5：參數 Shift 超出範圍。
 */
char UART_fgt(char LSByte, char Mask, char Shift, void *Data_p);

/**
 * @brief ASA UART put函式
 *
 * @ingroup hw_uart_func
 * @param LSByte 暫存器編號。
 * @param Bytes  資料大小。
 * @param Data_p 資料指標。
 * @return char  錯誤代碼：
 *   - 0：成功無誤。
 *   - 3：參數 Bytes 錯誤。
 */
char UART_put(char LSByte, char Bytes, void *Data_p);

/**
 * @brief ASA UART get函式
 *
 * @ingroup hw_uart_func
 * @param LSByte 暫存器編號。
 * @param Bytes  資料大小。
 * @param Data_p 資料指標。
 * @return char  錯誤代碼：
 *   - 0：成功無誤。
 *   - 3：參數 Bytes 錯誤。
 */
char UART_get(char LSByte, char Bytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_HARDWARE_UART_H
