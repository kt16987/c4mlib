/**
 * @file asa_twi.h
 * @author Yuchen
 * @date 2019.10.02
 * @brief 放置TWI通訊封包函式原型
 */

#ifndef C4MLIB_ASATWI_ASA_TWI_H
#define C4MLIB_ASATWI_ASA_TWI_H

#include "c4mlib/macro/src/std_res.h"

#include "twi.h"

/* Public Section Start */
/**
 * @brief ASA TWI Master 多位元組傳送函式。
 *
 * @ingroup asatwi_func
 * @param mode      TWI通訊模式，目前支援：1、2、3、4、5、6。
 * @param SLA       Slave(僕)裝置的TWI ID。
 * @param RegAdd    遠端讀寫暫存器(Register)的位址或控制旗標(control flag)。
 * @param Bytes     待送資料位元組數。
 * @param Data_p    待送資料指標。
 * @param WaitTick  位元組間延遲時間，單位為 1us。
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 1： Timeout。
 *                   - 4： SLA錯誤。
 *
 * ASA TWI Master Transmit 函式，依照功能分為6種Mode，通訊方式如下：
 *  - mode 1：
 *      TWI通訊第一 Bytes 為 [RegAdd | Data_p[Bytes-1]] ，並由高至低傳輸，
 *      使用者須自行將Data_p的最高位元向右位移RegAdd bits數。
 *      此mode中的RegAdd為控制旗標(control flag)。
 *      * 此 mode 中 RegAdd 為控制旗標(control flag)。
 *
 *  - mode 2：
 *      TWI通訊第一 Bytes 為 [RegAdd | Data_p[0]] ，並由低至高傳輸，
 *      使用者須自行將Data_p的最高位元向右位移RegAdd bits數。
 *      * 此 mode 中 RegAdd 為控制旗標(control flag)。
 *
 *  - mode 3：TWI通訊不指定RegAdd，並由高至低傳輸。
 *  - mode 4：TWI通訊不指定RegAdd，並由低至高傳輸。
 *  - mode 5：TWI通訊指定RegAdd，並由高至低傳輸。
 *  - mode 6：TWI通訊指定RegAdd，並由低至高傳輸。
 */
char TWIM_trm(char mode, char SLA, char RegAdd, char Bytes, uint8_t *Data_p, uint16_t WaitTick);

/**
 * @brief ASA TWI Master 多位元組接收函式。
 *
 * @ingroup asatwi_func
 * @param mode      TWI通訊模式，目前支援：1、2、3、4、5、6。
 * @param SLA       Slave(僕)裝置的TWI ID。
 * @param RegAdd    遠端讀寫暫存器(Register)的位址或控制旗標(control flag)。
 * @param Bytes     待收資料位元組數。
 * @param Data_p    待收資料指標。
 * @param WaitTick  位元組間延遲時間，單位為 1us。
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 1： Timeout。
 *                   - 4： SLA錯誤。
 *
 * ASA TWI Master receive 函式，依照功能分為6種Mode，通訊方式如下：
 *  - mode 1：TWI通訊第一Bytes為[RegAdd | Data_p[Bytes-1]]，並由高至低接收。
 *      * 此 mode 中 RegAdd 為控制旗標(control flag)。
 *  - mode 2：TWI通訊第一Bytes為[RegAdd | Data_p[0]]，並由低至高接收。
 *      * 此 mode 中 RegAdd 為控制旗標(control flag)。
 *  - mode 3：TWI通訊不指定RegAdd，並由高至低接收。
 *  - mode 4：TWI通訊不指定RegAdd，並由低至高接收。
 *  - mode 5：TWI通訊指定RegAdd，並由高至低接收。
 *  - mode 6：TWI通訊指定RegAdd，並由低至高接收。
 */
char TWIM_rec(char mode, char SLA, char RegAdd, char Bytes, uint8_t *Data_p,
              uint16_t WaitTick);

/**
 * @brief ASA TWI Master 旗標式傳送函式。
 *
 * @ingroup asatwi_func
 * @param mode      TWI通訊模式，目前支援：3、5。
 * @param SLA       Slave(僕)裝置的TWI ID。
 * @param RegAdd    遠端讀寫暫存器(Register)的位址或控制旗標(control flag)。
 * @param Mask      位元組遮罩。
 * @param Shift     待送旗標向左位移。
 * @param Data_p    待送資料指標。
 * @param WaitTick  位元組間延遲時間，單位為 1us。
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 1： Timeout。
 *                   - 4： SLA錯誤。
 *                   - 5： mode選擇錯誤。
 *
 * TWI Master(主)旗標式資料傳輸是由TWIM_trm()與TWIM_rec()實現，
 * 依照功能將只支援指定Mode，通訊方式如下：
 *  - mode 3：
 *      使用TWIM_rec() mode 3讀取資料，將資料左移後，用遮罩取資料，
 *      最後使用TWIM_trm() mode 3傳輸資料。
 *
 *  - mode 5：
 *      使用TWIM_rec() mode 5讀取指定RegAdd中的資料，將資料左移後，
 *      用遮罩取資料，最後使用TWIM_trm() mode 5傳輸資料到指定RegAdd。
 */
char TWIM_ftm(char mode, char SLA, char RegAdd, char Mask, char Shift,
              uint8_t *Data_p, uint16_t WaitTick);

/**
 * @brief ASA TWI Master 旗標式接收函式。
 *
 * @ingroup asatwi_func
 * @param mode      TWI通訊模式，目前支援：3、5。
 * @param SLA       Slave(僕)裝置的TWI ID。
 * @param RegAdd    遠端讀寫暫存器(Register)的位址或控制旗標(control flag)。
 * @param Mask      位元組遮罩。
 * @param Shift     接收旗標向右位移。
 * @param Data_p    待收資料指標。
 * @param WaitTick  位元組間延遲時間，單位為 1us。
 * @return  char    錯誤代碼：
 *                   - 0： 通訊成功。
 *                   - 1： Timeout。
 *                   - 4： SLA錯誤。
 *                   - 5： mode選擇錯誤。
 *
 * TWI Master(主)旗標式資料傳輸是由TWIM_rec()實現，
 * 依照功能將只支援指定Mode，通訊方式如下：
 *  - mode 3：使用TWIM_rec() mode 3讀取資料，將資料左移後，用遮罩取資料。
 *  - mode 5：
 *      使用TWIM_rec() mode 5讀取指定RegAdd中的資料，將資料左移後，
 *      用遮罩取資料。
 */
char TWIM_frc(char mode, char SLA, char RegAdd, char Mask, char Shift,
              uint8_t *Data_p, uint16_t WaitTick);

/**
 * @brief TWI Slave Mode 1 串列埠中斷執行片段
 *
 * @ingroup asatwi_func
 *
 * TWI Slave(僕) Mode 1 透過串列埠中斷來送收資料，其對應TWI Master(主) Mode 1，
 * 通訊方式如下：
 *  - TWI Slave Transmit：
 *      將RemoRW_reg所註冊的第一個Register(Reg_ID = 2)，由高至低傳輸。
 *  - TWI Slave Recive：
 *      將Master接收到的資料儲存到RemoRW_reg所註冊的第一個
 *      Register(Reg_ID = 2)，由高至低接收。
 *
 * @warning 此mode不支援廣播功能
 */
void ASA_TWIS1_step(void);

/**
 * @brief TWI Slave Mode 2 串列埠中斷執行片段
 *
 * @ingroup asatwi_func
 *
 * TWI Slave(僕) Mode 2 透過串列埠中斷來送收資料，其對應TWI Master(主) Mode 2，
 * 通訊方式如下：
 *  - TWI Slave Transmit：
 *      將RemoRW_reg所註冊的第一個Register(Reg_ID = 2)，由低至高傳輸。
 * 
 *  - TWI Slave Recive：
 *      將Master接收到的資料儲存到RemoRW_reg所註冊的第一個
 *      Register(Reg_ID = 2)，由低至高接收。
 *
 * @warning 此mode不支援廣播功能
 */
void ASA_TWIS2_step(void);

/**
 * @brief TWI Slave Mode 3 串列埠中斷執行片段
 *
 * @ingroup asatwi_func
 *
 * TWI Slave(僕) Mode 3 透過串列埠中斷來送收資料，其對應TWI Master(主) Mode 3，
 * 通訊方式如下：
 *  - TWI Slave Transmit：
 *      將RemoRW_reg所註冊的第一個Register(Reg_ID = 2)，由高至低傳輸。
 * 
 *  - TWI Slave Recive：
 *      將Master接收到的資料儲存到RemoRW_reg所註冊的第一個
 *      Register(Reg_ID = 2)，由高至低接收。
 *
 * @warning 此mode不支援廣播功能
 */
void ASA_TWIS3_step(void);

/**
 * @brief TWI Slave Mode 4 串列埠中斷執行片段
 *
 * @ingroup asatwi_func
 *
 * TWI Slave(僕) Mode 4 透過串列埠中斷來送收資料，其對應TWI Master(主) Mode 4，
 * 通訊方式如下：
 *  - TWI Slave Transmit：
 *      將RemoRW_reg所註冊的第一個Register(Reg_ID = 2)，由低至高傳輸。
 * 
 *  - TWI Slave Recive：
 *      將Master接收到的資料儲存到RemoRW_reg所註冊的第一個，
 *      Register(Reg_ID = 2)，由低至高接收。
 *  
 * @warning 此mode不支援廣播功能
 */
void ASA_TWIS4_step(void);

/**
 * @brief TWI Slave Mode 5 串列埠中斷執行片段
 *
 * @ingroup asatwi_func
 * TWI Slave(僕) Mode 5 透過串列埠中斷來送收資料，其對應TWI Master(主) Mode 5，
 * 通訊方式如下：
 *  - TWI Slave Transmit：
 *      將RemoRW_reg所註冊的指定Register，由高至低傳輸。
 * 
 *  - TWI Slave Recive：
 *      將 Master 接收到的資料儲存到 RemoRW_reg 所註冊的指定 
 *      Register，由高至低接收。
 */
void ASA_TWIS5_step(void);

/**
 * @brief TWI Slave Mode 6 串列埠中斷執行片段
 *
 * @ingroup asatwi_func
 * TWI Slave(僕) Mode 6 透過串列埠中斷來送收資料，其對應TWI Master(主) Mode 6，
 * 通訊方式如下：
 *  - TWI Slave Transmit：
 *      將RemoRW_reg所註冊的指定Register，由低至高傳輸。
 * 
 *  - TWI Slave Recive：
 *      將Master接收到的資料儲存到RemoRW_reg所註冊的指定
 *      Register，由低至高接收。
 */
void ASA_TWIS6_step(void);
/* Public Section End */

#endif  // C4MLIB_ASATWI_ASA_TWI_H
