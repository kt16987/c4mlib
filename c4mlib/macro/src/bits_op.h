/**
 * @file bits_op.h
 * @author LiYu87
 * @date 2019.01.28
 * @brief Provide standard bitswise operation.
 */

#ifndef C4MLIB_MACRO_BITS_OP_H
#define C4MLIB_MACRO_BITS_OP_H

/**
 * @defgroup macro_macro
 */

/* Public Section Start */
/**
 * @def SETBIT(ADDRESS, BIT)
 * @ingroup macro_macro
 * @brief 將 ADDRESS 指定的 BIT 設置為 1。
 */
#define SETBIT(ADDRESS, BIT) ((ADDRESS) |= (1 << BIT))

/**
 * @def CLRBIT(ADDRESS, BIT)
 * @ingroup macro_macro
 * @brief 將 ADDRESS 指定的 BIT 清除為 0。
 */
#define CLRBIT(ADDRESS, BIT) ((ADDRESS) &= ~(1 << BIT))

/**
 * @def CHKBIT(ADDRESS, BIT)
 * @ingroup macro_macro
 * @brief 檢查 ADDRESS 指定的 BIT 是 1 或 0。
 */
#define CHKBIT(ADDRESS, BIT) (((ADDRESS) & (1 << BIT)) == (1 << BIT))

/**
 * @def REGFPT(ADDRESS, MASK, SHIFT, DATA)
 * @ingroup macro_macro
 * @brief 依照指定的 MASK, SHIFT, DATA 去讀取暫存器 ADDRESS。
 */
#define REGFPT(ADDRESS, MASK, SHIFT, DATA) \
    ((ADDRESS) = (((ADDRESS) & (~MASK)) | (((DATA) << (SHIFT)) & (MASK))))

/**
 * @def REGFGT(ADDRESS, MASK, SHIFT, DATA_P)
 * @ingroup macro_macro
 * @brief 依照指定的 MASK, SHIFT, DATA_P 去寫入暫存器 ADDRESS。
 */
#define REGFGT(ADDRESS, MASK, SHIFT, DATA_P) \
    ((*((char*)DATA_P)) = (((ADDRESS) & (MASK)) >> (SHIFT)))
/* Public Section End */

#endif  // C4MLIB_MACRO_BITS_OP_H
