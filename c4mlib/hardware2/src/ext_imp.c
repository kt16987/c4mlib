/**
 * @file ext_imp.c
 * @author LiYu87
 * @date 2019.09.04
 * @brief 
 * 
 */

#if defined(__AVR_ATmega128__)
#    include "c4mlib/hardware2/src/m128/ext_imp.c"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "c4mlib/hardware2/src/m88/ext_imp.c"
#elif defined(__AVR_ATtiny2313__)
#    include "c4mlib/hardware2/src/tiny2313/ext_imp.c"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif
