/**
 * @file test_kb00_mode1.c
 * @author s915888
 * @brief 測試 kb00 的 ASCII 模式運作。
 * @date 2019.7.29
 */

#include "c4mlib/asakb00/src/kb00.h"
#include "c4mlib/device/src/device.h"

#include "c4mlib/config/asamodule.cfg"

int main() {
    C4M_DEVICE_set();

    AsaKb00Para_t KB00 = ASA_KB00_PARA_INI;

    ASA_KB00_set(4, 200, 0xff, 0, 1, &KB00);
    // 設定成 ASCII 模式

    char a;
    char res;
    while (1) {
        res = ASA_KB00_get(4, 100, 1, &a, &KB00);
        printf("res=%d, a=%c\n", res, a);
    }
}
