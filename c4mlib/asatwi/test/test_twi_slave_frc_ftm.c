/**
 * @file test_twi_slave_frc_ftm.c
 * @author YuChen
 * @brief TWI Slave flag receive transmit 測試程式
 * @date 2019.10.04
 *
 * 測試方式：<br>
 *      軟體：本測試程式需搭配test_twi_slave_frc_ftm.c同步測試。<br>
 *      硬體：須將兩塊ASA_M128單板電腦的SCL與SDA串接。<br>
 * 測試時須先將已燒錄test_twi_slave_mode5.c的ASA_M128啟動，再將燒錄test_twi_master_mode5.c的ASA_M128啟動。<br>
 *
 * 測試程式執行動作：<br>
 * 接收Master板的資料。
 *
 * 測試結果：<br>
 * >>  Create RemoRWreg [2] with 1 bytes<br>
 * >>  Create RemoRWreg [3] with 1 bytes<br>
 * >>  DEVICE ID = 39
 */

#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/asatwi/src/twi.h"
#include "c4mlib/device/src/device.h"

#include <avr/interrupt.h>

#include "c4mlib/config/remo_reg.cfg"

void TWI_Slave_set();

int main(void) {
    C4M_STDIO_init();
    C4M_DEVICE_set();
    TWI_Slave_set(); /*Slave Setting*/
    SerialIsr_t TWIIsrStr = SERIAL_ISR_STR_TWI_INI; /*初始化TWI Remo_reg結構資料*/
    // FIXME: 使用新版初始化
    SerialIsr_net(&TWIIsrStr, 0);
    uint8_t reg_1[1] = {0}; /*建立Slave端暫存器*/
    uint8_t reg_2[1] = {0}; /*建立Slave端暫存器*/
    uint8_t reg_1_ID = RemoRW_reg(&TWIIsrStr, reg_1, 1);  /*註冊暫存器並取得暫存器ID(對應Master函式RegAdd傳參)*/
    uint8_t reg_2_ID = RemoRW_reg(&TWIIsrStr, reg_2, 1);  /*註冊暫存器並取得暫存器ID(對應Master函式RegAdd傳參)*/
    printf("Create RemoRWreg [%u] with %u bytes\n", reg_1_ID,
           TWIIsrStr.remo_reg[reg_1_ID].sz_reg);
    printf("Create RemoRWreg [%u] with %u bytes\n", reg_2_ID,
           TWIIsrStr.remo_reg[reg_2_ID].sz_reg);
    printf("DEVICE ID = %x\n", TWAR);
    sei(); /*Enable interrup*/
    while (1)
        ;
}
ISR(TWI_vect) {
    ASA_TWIS5_step();
}

void TWI_Slave_set() {
    TWAR = (ASAConfigStr_inst.ASA_ID |= 0b00000001);
    // Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ;
    // prescaler bits = 1
    TWBR = 12;
    // enable TWI TWI_Interrupt and shack_hand for Master Start signal
    TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWEA);
}
