/**
 * @file test_timint.c
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief 提供ASA函式庫標準中斷介面測試程式，請確認PA0~PA3的腳位為方波訊號。
 */

#define USE_C4MLIB_DEBUG
#define F_CPU 11059200UL

#include "c4mlib/debug/src/debug.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/tim.h"
#include "c4mlib/interrupt/src/intfreqdiv.h"
#include "c4mlib/time/src/hal_time.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>

#include "c4mlib/config/tim.cfg"

/**
 * @brief : When timer interrrupt, it will toggle hardware IO. Each timer will call the same function, but parameters are different.
 * timer0 0 : PA0
 * timer1 1 : PA1
 * timer2 2 : PA2
 * timer3 3 : PA3
 */

TimIntStr_t TimInt0 = TIMINT_0_STR_INI;
TimIntStr_t TimInt1 = TIMINT_1_STR_INI;
TimIntStr_t TimInt2 = TIMINT_2_STR_INI;
TimIntStr_t TimInt3 = TIMINT_3_STR_INI;

/* Setting TIMER hardware function start. */
void init_timer0();
void init_timer1();
void init_timer2();
void init_timer3();
/* Setting TIMER hardware function end. */

/* User define function, when TIMER interrupt, it will call. */
void USER_FUNCTION(void* pvData_p);

int main() {

    C4M_STDIO_init();
    ASABUS_ID_init();

    /***** Test the  TimInt component *****/
    printf("======= Test TimInt component =======\n");

    printf("Setup PORTA[0:7] output\n");
    DDRA = 0xFF;

    printf("Initial Start\n");
    TimIntStr_t tim0 = TIM0_INIT;
    TimIntStr_t tim1 = TIM1_INIT;
    TimIntStr_t tim2 = TIM2_INIT;
    TimIntStr_t tim3 = TIM3_INIT;

    TimInt_net(&tim0, 0);  // 連結變數 tim0 到 tim0
    TimInt_set(&tim0);
    TimInt_net(&tim1, 1);  // 連結變數 tim0 到 tim0
    TimInt_set(&tim1);
    TimInt_net(&tim2, 2);  // 連結變數 tim0 到 tim0
    TimInt_set(&tim2);
    TimInt_net(&tim3, 3);  // 連結變數 tim0 到 tim0
    TimInt_set(&tim3);

    // Initialize the connect TimIntStr_t with Internal TIM_isr();
    TimInt_net(&TimInt0, 0);
    TimInt_net(&TimInt1, 1);
    TimInt_net(&TimInt2, 2);
    TimInt_net(&TimInt3, 3);

    uint8_t fg_interrupt0 = 0;
    uint8_t fg_interrupt1 = 1;
    uint8_t fg_interrupt2 = 2;
    uint8_t fg_interrupt3 = 3;

     /* Register the USER_FUNCTION to TIMER interrupt, and fg_interruptn is the
     * parameters that user feed. */
    uint8_t time_id0 = TimInt_reg(&TimInt0, USER_FUNCTION, &fg_interrupt0);
    printf("Added USER_FUNCTION with fg_interrupt0  to TimInt0 Component\n");
    uint8_t time_id1 = TimInt_reg(&TimInt1, USER_FUNCTION, &fg_interrupt1);
    printf("Added USER_FUNCTION with fg_interrupt1  to TimInt1 Component\n");
    uint8_t time_id2 = TimInt_reg(&TimInt2, USER_FUNCTION, &fg_interrupt2);
    printf("Added USER_FUNCTION with fg_interrupt2  to TimInt2 Component\n");
    uint8_t time_id3 = TimInt_reg(&TimInt3, USER_FUNCTION, &fg_interrupt3);
    printf("Added USER_FUNCTION with fg_interrupt3  to TimInt3 Component\n");

    TimInt_en(&TimInt0, time_id0, true);
    TimInt_en(&TimInt1, time_id1, true);
    TimInt_en(&TimInt2, time_id2, true);
    TimInt_en(&TimInt3, time_id3, true);

    printf("Enable Global Interrupt\n");
    /** Enable interrupts */
    sei();
    DEBUG_INFO("Timer 0~3 start, check IO work successfully\n");
    while (1) {
        DEBUG_INFO("TCCR0:%x, TCCR1B:%x, TCCR2:%x, TCCR3B:%x\n", TCCR0, TCCR1B, TCCR2, TCCR3B);
        _delay_ms(3000);
    }
}

/**
 * @brief: Hardware timer setting
 * Prescaler: 1024
 * Period: 10 ms
 * mode: CTC
 */

// Initialize the TIME0 with CTC mode, interrupt at 100 Hz
void init_timer0() {
    OCR0 = 53;
    TCCR0 = 0b00001111;
    TCNT0 = 0;
    TIMSK |= 1 << OCIE0;
}

// Initialize the TIME1 with CTC mode, interrupt at 100 Hz
void init_timer1() {
    OCR1A = 53;
    TCCR1A = 0b00000000;
    TCCR1B = 0b00001101;
    TCNT1 = 0x00;
    TCNT1 = 0;
    TIMSK |= 1 << OCIE1A;
}

// Initialize the TIME2 with CTC mode, interrupt at 100 Hz
void init_timer2() {
    OCR2 = 53;
    TCCR2 = 0b000001101;
    TCNT2 = 0;
    TIMSK |= 1 << OCIE2;
}

// Initialize the TIME3 with CTC mode, interrupt at 100 Hz
void init_timer3() {
    OCR3A = 53;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00001101;
    TCNT3 = 0;
    ETIMSK |= 1 << OCIE3A;
}

void USER_FUNCTION(void* pvData_p) {
    uint8_t* fg_interrupt = (uint8_t*)pvData_p;
    // DEBUG_INFO("Run %d\n", *fg_interrupt);
    if (fg_interrupt != NULL) {
        switch (*fg_interrupt) {
            case 0:
                PORTA ^= 1;
                break;
            case 1:
                PORTA ^= 2;
                break;
            case 2:
                PORTA ^= 4;
                break;
            case 3:
                PORTA ^= 8;
                break;
        }
    }
}
