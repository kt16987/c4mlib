# Git usage style

主寫者： [TWNET](https://gitlab.com/TWNET) <br>
編輯者： <br>
日期：  <br>

##  Commit註解規範

### 重要
- 用一行空白行分隔標題與內容
- Commit內容請用中文，除專用名詞

### 標題規則
- 標題不以句點結尾
- 限制標題最多只有 50 字元 (包含全形及半形)
- 以命令語氣撰寫標題  
    舉例:
    - 修正時間錯亂問題
    - 新增記憶體管理執行緒
    - 更新API說明文件

### 內文規則
- 內文每行最多 72 字
- 用內文解釋 what 以及 why vs. how
- 若此commit有關連到某個ISSUE，請在最後一行加上`ISSUE: #編號`，如下面 Case: 一般情況
- 盡量對每個有修改到的地方做簡短的說明

## 1. Case: 一般情況
```
新增測試程式:Timeout與HAL_Time

修正hal_time.h內函式原型宣告錯誤
修正asa_timeout.h TimeoutCollector_ini() -> Timeout_ini();
修正Timeout_tick()回傳至void
修正Timeout_reg()未回傳timeout ISR ID問題
新增上傳rule至common/makefile

新增測試項目
    1. hal_time
    2. asa_timeout

ISSUE: #21
```

## 2. Case: 一般情況
```
測試並實作完成UARTM mode0 trm()與rec()
測試並實作完成UARTS mode0 RemoRW_reg()與UARTS_rx()

修正UARTM:
    1. M128_UART_set()內實作修改至REGFPT()標準用法
    2. 修正M128_UARTM_trm()中，Checksum計算到HEADER
    3. 分離主從的HEADER封包分別為 CMD_HEADER <0xAA> 與 RSP_HEADER (0xAB)，
    避免從裝置之間互相觸發。
    4. 修正M128_UARTM_rec()中，Checksum檢查正確後沒有將暫時暫存區寫入至目標記憶體位置。

修正UARTS:
    1. 修正狀態機至最新版本2018/11/29，主要解決了逾時觸發時機不正確。
    2. 修正寫入模式結束後無回應問題，(增加回應HEADER片段)

已知問題:
    1. 目前採用臨時方法解決UARTS_tx()實作，狀態機逾時重複觸發問題造成無法正確實作，故採用連續步驟實作。
    2. M128 與 M88所採用的UARTn分別為1,0，造成程式移植上困難，已加入#ifdef 選擇性程式碼片段暫時解決衝突。
    3. 尚未加入public註解提供python進行解析
    4. 尚未加入UARTS_rx()逾時測試程式，程式已完成但無進行此測試。
    5. UART_ID無登陸機制，採用Hardcoding方式完成，需進行UART_ID複寫機制實作。

ISSUE: #21, #18, #5
PR: !10
```

## 3. Case: 修改非常少，一句話即可括之
```
修正時間錯亂問題
```
