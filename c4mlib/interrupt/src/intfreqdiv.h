/**
 * @file intfreqdiv.h
 * @author Ye cheng-Wei
 * @author LiYu87
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @brief 提供ASA函式庫中斷除頻功能，將提供標準step()函式呼叫，
 * 根據已設定之cycle與phase，提供條件式觸發呼叫功能。
 */

#ifndef C4MLIB_COMMON_ASA_INTFREQDIV_H
#define C4MLIB_COMMON_ASA_INTFREQDIV_H

#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

#include "c4mlib/config/interrupt.cfg"

/**
 * @defgroup interrupt_macro  interrupt macros
 * @defgroup interrupt_struct interrupt structs
 * @defgroup interrupt_func   interrupt functions
 */

/* Public Section Start */
/**
 * @brief IFD工作結構
 * @ingroup interrupt_struct
 *
 * 負責管理一項工作的觸發週期、相位、計數等參數。
 * 為IFD管理器底下使用的結構之一。
 */
typedef struct {
    volatile uint16_t cycle;    ///< 計數觸發週期。
    volatile uint16_t phase;    ///< 計數觸發相位。
    volatile uint16_t counter;  ///< 計數器，計數器固定頻率上數。
    volatile uint8_t enable;  ///< 禁致能控制，決定本逾時ISR是否致能。
    volatile Func_t func_p;  ///< 執行函式指標，為無回傳、傳參為void*函式。
    void* funcPara_p;  ///< 中斷中執行函式專用結構化資料住址指標。
} IntFreqDivISR_t;

/**
 * @brief IFD管理器結構
 * @ingroup interrupt_struct
 *
 * 提供給中斷除頻功能，Interrupt Frequence Diveder(IFD)相關函式使用的結構。
 * 負責管理登記好的IFD工作。
 */
typedef struct {
    uint8_t total;  ///< 紀錄已註冊IFD工作數量
    volatile IntFreqDivISR_t fb[MAX_IFD_FUNCNUM];  ///< 紀錄所有已註冊IFD工作
} IntFreqDivStr_t;

/**
 * @brief 初始化IFD管理器
 *
 * @param IntFreqDivStr_p IFD管理器結構指標
 */
void IntFreqDiv_net(IntFreqDivStr_t* IntFreqDivStr_p);

/**
 * @brief 註冊一項工作到IFD管理器中。
 * @ingroup interrupt_func
 *
 * @param IntFreqDivStr_p IFD管理器的指標。
 * @param FbFunc_p 要註冊的函式，為無回傳、傳參為void*函式。
 * @param FbPara_p 要註冊函式的傳參。
 * @param cycle 循環週期。
 * @param phase 觸發相位，循環週期中的第幾次計數觸發。
 * @return uint8_t IFD工作編號。
 *
 * 此函式會在IFD管理器建立一項IFD工作，並會回傳其在管理器中的工作編號。
 * IFD工作預設為關閉，可以使用 IntFreqDiv_en 開啟。當 IntFreqDiv_step
 * 執行一次時，IFD工作的計數器便會上數，並依據參數設定的循環週期、
 * 觸發相位來決定要不要觸發並執行IFD工作。
 */
uint8_t IntFreqDiv_reg(IntFreqDivStr_t* IntFreqDivStr_p, Func_t FbFunc_p,
                       void* FbPara_p, uint16_t cycle, uint16_t phase);

/**
 * @brief 啟用/關閉指定的IFD工作。
 * @ingroup interrupt_func
 *
 * @param IntFreqDivStr_p IFD管理器的指標。
 * @param Fb_Id 要啟用的IFD工作編號。
 * @param enable 是否啟用，1:啟用、0:關閉。
 *
 * 此函式可以控制一項IFD工作要不要啟用，依照工作編號去開關IFD管理器中相對應編號
 * 的IFD工作。啟用後IFD工作中的計數器才會開始計數，也才能被觸發。
 *
 * 若輸入的編號還沒有被註冊，將不會有任何動作。
 */
void IntFreqDiv_en(IntFreqDivStr_t* IntFreqDivStr_p, uint8_t Fb_Id,
                   uint8_t enable);

/**
 * @brief 透過IFD管理器計數一次。
 * @ingroup interrupt_func
 *
 * @param IntFreqDivStr_p IFD管理器的指標。
 *
 * 此函式會去計數IFD管理器供登記並啟用的IFD工作，若計數大小與觸發相位相等時，
 * 便會執行IFD工作。
 *
 * 此函式可以被註冊到硬體中斷中，如此一來，中斷觸發時便會計數並觸發工作。也可
 * 以透過手動呼叫此函式來計數。
 */
void IntFreqDiv_step(IntFreqDivStr_t* IntFreqDivStr_p);
/* Public Section End */

#endif  // C4MLIB_COMMON_ASA_INTFREQDIV_H
