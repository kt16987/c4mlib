/**
 * @file test_ex_array.c
 * @author LiYu87
 * @brief 測試函式 HMI_get_struct
 * @date 2019.08.21
 *
 * 建立在 HMI_put_struct 可以成功發送資料的前提下，進行測試。
 * 建議配合人機進行測試。
 */

#include "c4mlib/asahmi/src/asa_hmi.h"
#include "c4mlib/device/src/device.h"

struct st {
    uint8_t a[5];
    uint16_t b[5];
};

int main() {
    C4M_STDIO_init();

    struct st data = {
        .a = {0,1,2,3,4},
        .b = {5,6,7,8,9}
    };

    HMI_put_struct("ui8_5,ui16_5", sizeof(struct st), &data);
    HMI_get_struct("ui8_5,ui16_5", sizeof(struct st), &data);
    HMI_put_struct("ui8_5,ui16_5", sizeof(struct st), &data);

    return 0;
}
