/**
 * @file adc.c
 * @author LiYu87
 * @date 2019.07.15
 * @brief adc 暫存器操作相關函式實作。
 */

#include "c4mlib/hardware/src/adc.h"

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

char ADC_fpt(char LSByte, char Mask, char Shift, char Data) {
    if (Shift <= 7) {
        switch (LSByte) {
            case 200:
                REGFPT(ADMUX, Mask, Shift, Data);
                break;
            case 201:
                REGFPT(ADCSRA, Mask, Shift, Data);
                break;
            case 202:
                REGFPT(DDRF, Mask, Shift, Data);
                break;
            default:
                return RES_ERROR_LSBYTE;
        }
    }
    else {
        return RES_ERROR_SHIFT;
    }
    return RES_OK;
}

char ADC_fgt(char LSByte, char Mask, char Shift, void* Data_p) {
    if (Shift <= 7) {
        switch (LSByte) {
            case 201:
                REGFGT(ADCSRA, Mask, Shift, Data_p);
                break;
            default:
                return RES_ERROR_LSBYTE;
        }
    }
    else {
        return RES_ERROR_SHIFT;
    }
    return RES_OK;
}

char ADC_put(char LSByte, char Bytes, void* Data_p) {
    return RES_OK;
}

char ADC_get(char LSByte, char Bytes, void* Data_p) {
    // TODO: ADMUX bytes error ?
    switch (LSByte) {
        case 1: {
            if (Bytes == 1) {
                *((char*)Data_p) = ADCH; 
            }
            else {
                return RES_ERROR_BYTES;
            }
            break;
        }
        case 2: {
            if (Bytes == 2) {
                *((char*)Data_p) = ADCL;
                *((char*)Data_p + 1) = ADCH;               
            }
            else{
                return RES_ERROR_BYTES;
            }
            break;
        }
        default:
            return RES_ERROR_LSBYTE;
    }    
    return RES_OK;
}
