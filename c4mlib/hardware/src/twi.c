/**
 * @file twi.c
 * @author LiYu87
 * @date 2019.07.15
 * @brief twi 暫存器操作函式各硬體實作分割。
 */

#if defined(__AVR_ATmega128__)
#    include "m128/twi.c"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/twi.c"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/twi.c"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif
