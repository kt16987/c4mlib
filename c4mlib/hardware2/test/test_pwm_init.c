/**
 * @file test_pwm_init.c
 * @author LiYu87
 * @date 2019.09.05
 * @brief
 *
 * 確認 TIMER 0 1 2 3 暫存器有改變，確認 set 與 initFunc 有正常連接。
 * 測試裝置：ASA_M128_V2
 * 測試硬體：TIMER 0 1 2 3
 */

#include "c4mlib/device/src/device.h"
#include "c4mlib/hardware2/src/pwm.h"

#include "c4mlib/config/pwm.cfg"

int main(void) {
    C4M_DEVICE_set();

    printf("PWM init test ------------------------------------------------\n");

    PwmIntStr_t pwm0 = PWM0_INIT;
    PwmIntStr_t pwm1 = PWM1_INIT;
    PwmIntStr_t pwm2 = PWM2_INIT;
    PwmIntStr_t pwm3 = PWM3_INIT;

    PwmInt_net(&pwm0, 0);
    PwmInt_set(&pwm0);
    PwmInt_net(&pwm1, 1);
    PwmInt_set(&pwm1);
    PwmInt_net(&pwm2, 2);
    PwmInt_set(&pwm2);
    PwmInt_net(&pwm3, 3);
    PwmInt_set(&pwm3);

    printf("TIMSK  = 0x%02x", TIMSK);
    printf("ETIMSK = 0x%02x", TIMSK);

    printf("OCR0   = 0x%02x\n",OCR0);
    printf("TCCR0  = 0x%02x\n",TCCR0);
    printf("TCNT0  = 0x%02x\n",TCNT0);

    printf("OCR1A  = 0x%02x\n",OCR1A);
    printf("TCCR1A = 0x%02x\n",TCCR1A);
    printf("TCCR1B = 0x%02x\n",TCCR1B);
    printf("TCCR1C = 0x%02x\n",TCCR1C);
    printf("TCNT1  = %d\n",TCNT1);
    
    printf("OCR2   = 0x%02x\n",OCR2);
    printf("TCCR2  = 0x%02x\n",TCCR2);
    printf("TCNT2  = 0x%02x\n",TCNT2);

    printf("OCR3A  = 0x%02x\n",OCR3A);
    printf("TCCR3A = 0x%02x\n",TCCR3A);
    printf("TCCR3B = 0x%02x\n",TCCR3B);
    printf("TCCR3C = 0x%02x\n",TCCR3C);
    printf("TCNT3  = %d\n", TCNT3);

    return 0;
}
