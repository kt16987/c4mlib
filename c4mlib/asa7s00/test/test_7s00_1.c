/**
 * @file test_7s00_1.c
 * @author LiYu87
 * @brief 測試7S00基本顯示
 * @date 2019.08.06
 */

#include "c4mlib/asa7s00/src/7s00.h"
#include "c4mlib/device/src/device.h"

#include "c4mlib/config/asamodule.cfg"

extern Asa7s00Para_t ASA_7S00_str;

int main() {
    C4M_DEVICE_set();

    uint8_t data[4] = {0, '-', '1', '2'};
    uint8_t res = 0;

    res = ASA_7S00_put(2, 0, 4, data, &ASA_7S00_str);
    printf("res0 = %d\n", res);
}
