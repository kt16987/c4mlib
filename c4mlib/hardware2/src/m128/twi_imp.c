/**
 * @file twi_imp.c
 * @author 
 * @brief 
 * @date 2019.09.04
 * 
 */

#include "c4mlib/hardware2/src/m128/twi_imp.h"

#include "c4mlib/hardware2/src/twi.h"
#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

#include "c4mlib/config/twi.cfg"

static uint8_t twi_hw_init(TwiIntStr_t *IntStr_p);

#define TWI_SLAVE_ID 0x78   // TODO 看是否能改成 user 自己宣告

TwiIntStr_t TwiImp[TWI_HW_NUM] = {
    {
        .TwiSet = TWI_HW_SET_CFG,
        .SetFunc_p = twi_hw_init
    }
};

TwiIntStr_t *TwiIntStrList_p[TWI_HW_NUM];

uint8_t twi_hw_init(TwiIntStr_t *IntStr_p) {
    TWBR = IntStr_p->TwiSet.BitRatePara0 ;
    switch (IntStr_p->TwiSet.TxRxEn) {
        case DISABLE :
            TWCR &= ~(1 << TWEN);
            TWCR |= (0 << TWEN) ;
            break;
        case ENABLE:
            TWCR &= ~(1 << TWEN);
            TWCR |= (1 << TWEN) ;
            break;
    }
    switch (IntStr_p->TwiSet.IntEn){
        case DISABLE :
            TWCR &= ~(1 << TWIE);
            TWCR |= (0 << TWIE) ;
            break;
        case ENABLE :
            TWCR &= ~(1 << TWIE);
            TWCR |= (1 << TWIE) ;
            break;
    }

    switch (IntStr_p->TwiSet.BroadCast){
        case DISABLE :
            TWAR &= ~(1 << TWGCE);
            TWAR |= (0 << TWGCE) ;
            break;
        case ENABLE :
            TWAR &= ~(1 << TWGCE);
            TWAR |= (1 << TWGCE) ;
            break;
    }
    switch (IntStr_p->TwiSet.BitRatePara1){
        case 0 :
            TWSR &= ~((1 << TWPS1)|(1 << TWPS0));
            TWSR |= ((0 << TWPS1)|(0 << TWPS0)) ;
            break;
        case 1 :
            TWSR &= ~((1 << TWPS1)|(1 << TWPS0));
            TWSR |= ((0 << TWPS1)|(1 << TWPS0)) ;
            break;
        case 2 :
            TWSR &= ~((1 << TWPS1)|(1 << TWPS0));
            TWSR |= ((1 << TWPS1)|(0 << TWPS0)) ;
            break;
        case 3 :
            TWSR &= ~((1 << TWPS1)|(1 << TWPS0));
            TWSR |= ((1 << TWPS1)|(1 << TWPS0)) ;
            break;
    default:
        break;
    }

    return 0 ;
}
