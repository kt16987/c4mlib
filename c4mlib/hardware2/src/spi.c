/**
 * @file spiint.c
 * @author Deng Xiang-Guan
 * @author LiYu87
 * @date 2019.09.04
 * @brief
 *
 */

#include "c4mlib/hardware2/src/spi.h"

#include "c4mlib/debug/src/debug.h"
#include "c4mlib/hardware2/src/spi_imp.h"
#include "c4mlib/macro/src/std_res.h"

uint8_t SpiInt_net(SpiIntStr_t* IntStr_p, uint8_t Num) {
    if(IntStr_p != NULL) {
        if(Num < SPI_HW_NUM) {
            SpiIntStrList_p[Num] = IntStr_p;

            IntStr_p->SetFunc_p = SpiImp[Num].SetFunc_p;

            DEBUG_INFO("Succeed to net SPI number %d\n", Num);
            return INT_OK;
        }
        else {
            // warning Num is excessive, IntStr_p can not net.
            DEBUG_INFO("Warning Num is excessive, IntStr_p can not net.");
            return INT_ERROR_NUM_EXCEED;
        }
    }
    return INT_ERROR_POINTER_IS_NULL;
}

uint8_t SpiInt_set(SpiIntStr_t* IntStr_p) {
    if (IntStr_p != NULL) {
        return IntStr_p->SetFunc_p(IntStr_p);
    }
    else {
        return INT_ERROR_POINTER_IS_NULL;
    }
}

uint8_t SpiInt_reg(SpiIntStr_t* IntStr_p, Func_t FbFunc_p, void* FbPara_p) {
    if (IntStr_p != NULL) {
        uint8_t new_Id = IntStr_p->IntTotal;
        IntStr_p->IntFb[new_Id].enable = 0;  // Default Disable
        IntStr_p->IntFb[new_Id].func_p = FbFunc_p;
        IntStr_p->IntFb[new_Id].funcPara_p = FbPara_p;
        IntStr_p->IntTotal++;

        return new_Id;
    }
    else {
        DEBUG_INFO("Warning !!! IntStr_p is NULL\n");
        return INT_ERROR_POINTER_IS_NULL;
    }
}

void SpiInt_en(SpiIntStr_t* IntStr_p, uint8_t Fb_Id, uint8_t enable) {
    if (IntStr_p != NULL) {
        if (Fb_Id < IntStr_p->IntTotal) {
            IntStr_p->IntFb[Fb_Id].enable = enable;
        }
    }
    else {
        DEBUG_INFO("Warning !!! IntStr_p is NULL\n");
    }
}
