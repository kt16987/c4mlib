/**
 * @file isr_func.h
 * @author LiYu87
 * @author Deng Xiang-Guan
 * @date 2019.08.08
 * @priority 0
 * @brief 提供標準的 ISR FUNCTION 型態。
 */
#ifndef C4MLIB_INTERRUPT_ISR_FUNC_H
#define C4MLIB_INTERRUPT_ISR_FUNC_H

#include "c4mlib/macro/src/std_type.h"

/* Public Section Start */
/**
 * @brief FuncBlock 工作管理結構
 * @ingroup interrupt_struct
 *
 * 此結構被應用在硬體中斷、IFD中斷中，為一項工作。
 * 結構中包含該工作是否開啟、觸發時要執行哪個函式、以及該函式的傳參。
 */
typedef struct {
    volatile uint8_t enable;  ///< 禁致能
    Func_t func_p;            ///< 觸發時執行函式
    void* funcPara_p;         ///< 觸發時執行函式之傳參
} FuncBlockStr_t;
/* Public Section End */

#endif  // C4MLIB_INTERRUPT_ISR_FUNC_H
