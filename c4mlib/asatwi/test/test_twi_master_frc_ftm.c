/**
 * @file test_twi_master_frc_ftm.c
 * @author YuChen
 * @brief TWI Master flag receive transmit 測試程式
 * @date 2019.10.04
 *
 * 測試程式執行動作：<br>
 * 將欲設資料傳入Slave板中並讀回。<br>
 * 傳輸資料：temp_data1 = 0x05, temp_data2 = 0x50。<br>
 * 接收資料：ans1 = 0x05, ans2 = 0x50。
 *
 * 測試方式：<br>
 *      軟體：本測試程式需搭配test_twi_slave_frc_ftm.c同步測試。<br>
 *      硬體：須將兩塊ASA_M128單板電腦的SCL與SDA串接。
 *
 * 測試時須先將已燒錄test_twi_slave_mode5.c的ASA_M128啟動，再將燒錄test_twi_master_mode5.c的ASA_M128啟動。<br>
 * 測試結果：<br>
 * >>  [Master] Transmit Check = 0<br>
 * >>  [Master] Transmit Check = 0<br>
 * >>  [Master] Transmit Check = 0<br>
 * >>  [Master] Transmit Check = 0<br>
 * >>  [Master] Transmit Check = 0<br>
 * >>  [Master] Transmit Check = 0<br>
 * >>  [Master] Tramsmit frc = 5<br>
 * >>  [Master] Tramsmit frc = 50
 */

#include "c4mlib/asatwi/src/asa_twi.h"
#include "c4mlib/device/src/device.h"

#define ARRARY_SIZE 10
#define SLA 0x39
#define Test_mode 5
#define WAITTICK 50

void TWI_Master_set();
char check = 0;

int main(void) {
    C4M_STDIO_init();
    TWI_Master_set();

    uint8_t regadd2 = 0x02;
    uint8_t regadd3 = 0x03;
    uint8_t test_data1 = 0x0f;
    uint8_t test_data2 = 0xf0;
    uint8_t temp_data1 = 0x05;
    uint8_t temp_data2 = 0x50;
    uint8_t ans1 = 0;
    uint8_t ans2 = 0;
    check = TWIM_trm(Test_mode, SLA, regadd2, 1, &test_data1, WAITTICK);
    printf("[Master] Transmit Check = %d\n", check);
    check = TWIM_trm(Test_mode, SLA, regadd3, 1, &test_data2, WAITTICK);
    printf("[Master] Transmit Check = %d\n", check);
    check = TWIM_ftm(Test_mode, SLA, regadd2, 0x0f, 0, &temp_data1, WAITTICK);
    printf("[Master] Transmit Check = %d\n", check);
    check = TWIM_ftm(Test_mode, SLA, regadd3, 0xf0, 0, &temp_data2, WAITTICK);
    printf("[Master] Transmit Check = %d\n", check);
    check = TWIM_frc(Test_mode, SLA, regadd2, 0x0f, 0, &ans1, WAITTICK);
    printf("[Master] Transmit Check = %d\n", check);
    check = TWIM_frc(Test_mode, SLA, regadd3, 0xf0, 0, &ans2, WAITTICK);
    printf("[Master] Transmit Check = %d\n", check);
    printf("[Master] Tramsmit frc = %x\n", ans1);
    printf("[Master] Tramsmit frc = %x\n", ans2);
}
void TWI_Master_set() {
    // Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ;
    // prescaler bits = 1
    TWBR = 12;
}
