/**
 * @file adc_imp.c
 * @author LiYu87
 * @date 2019.09.04
 * @brief m128 adc 實現
 *
 */

#include "c4mlib/hardware2/src/m128/adc_imp.h"

#include "c4mlib/hardware2/src/adc.h"
#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_def.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

#include "c4mlib/config/adc.cfg"

static uint8_t adc0_set(AdcIntStr_t *IntStr_p);
static uint8_t adc0_trig(AdcIntStr_t *IntStr_p);
static uint8_t adc0_isDone(AdcIntStr_t *IntStr_p);
static uint8_t adc0_en(AdcIntStr_t *IntStr_p, uint8_t En);
static uint8_t adc0_getConv(AdcIntStr_t *IntStr_p, void *data_p);

AdcIntStr_t AdcImp[ADC_HW_NUM] = {
    {
        .AdcSet = ADC0_HW_SET_CFG,
        .SetFunc_p = adc0_set,
        .EnFunc_p = adc0_en,
        .TrigFunc_p = adc0_trig,
        .IsDoneFunc_p = adc0_isDone,
        .GetConvFunc_p = adc0_getConv
    }
};

// AdcIntStrList_p is used in std_isr
AdcIntStr_t *AdcIntStrList_p[ADC_HW_NUM];

#define IS_ADC_CHENNEL(CHENNEL) (((CHENNEL)&0b11100000) == 0)

#define IS_ADC_CLOCKPRESCALER(DIV)                                       \
    ((DIV) == ADC_CLOCKPRESCALER_2 || (DIV) == ADC_CLOCKPRESCALER_4 ||   \
     (DIV) == ADC_CLOCKPRESCALER_6 || (DIV) == ADC_CLOCKPRESCALER_8 ||   \
     (DIV) == ADC_CLOCKPRESCALER_16 || (DIV) == ADC_CLOCKPRESCALER_32 || \
     (DIV) == ADC_CLOCKPRESCALER_64 || (DIV) == ADC_CLOCKPRESCALER_128)

#define IS_ADC_RESOLUTION(RESOLUTION) \
    ((RESOLUTION) == ADC_RESOLUTION_10)

#define IS_ADC_REFERENCE(REF) \
    ((REF) == ADC_REF_AREF || (REF) == ADC_REF_AVCC || (REF) == ADC_REF_2560MV)

#define IS_ADC_DATAALIGN(ALIGN) \
    ((ALIGN) == ADC_DATAALIGN_RIGHT || (ALIGN) == ADC_DATAALIGN_LEFT)

#define IS_ADC_CONVMODE(CONVMODE)              \
    ((CONVMODE) == ADC_CONVMODE_SUCCESIVELY || \
     (CONVMODE) == ADC_CONVMODE_TRIGERED)

// mux setting is in register ADMUX
#define ADC_MUX_MASK _BV(MUX0) | _BV(MUX1) | _BV(MUX2) | _BV(MUX3) | _BV(MUX4)
#define ADC_MUX_SHIFT MUX0

// prescaler setting is in register ADCSRA
#define ADC_PS_MASK _BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2)
#define ADC_PS_SHIFT ADPS0

// ref setting is in register ADMUX
#define ADC_REF_MASK _BV(REFS0) | _BV(REFS1)
#define ADC_REF_SHIFT REFS0

uint8_t adc0_set(AdcIntStr_t *IntStr_p) {
    if (!IS_ENABLE_OR_DISABLE(IntStr_p->AdcSet.AdcConvEn)) {
        return 1;
    }
    if (!IS_ADC_CHENNEL(IntStr_p->AdcSet.Channel)) {
        return 2;
    }
    if (!IS_ADC_CLOCKPRESCALER(IntStr_p->AdcSet.ClockPrescaler)) {
        return 3;
    }
    if (!IS_ADC_RESOLUTION(IntStr_p->AdcSet.Resolution)) {
        return 4;
    }
    if (!IS_ADC_DATAALIGN(IntStr_p->AdcSet.DataAlign)) {
        return 5;
    }
    if (!IS_ADC_REFERENCE(IntStr_p->AdcSet.Reference)) {
        return 6;
    }
    if (!IS_ENABLE_OR_DISABLE(IntStr_p->AdcSet.IntEn)) {
        return 7;
    }
    if (!IS_ADC_CONVMODE(IntStr_p->AdcSet.IntEn)) {
        return 8;
    }

    switch (IntStr_p->AdcSet.AdcConvEn) {
        case ENABLE: {
            SETBIT(ADCSRA, ADEN);
            break;
        }
        case DISABLE: {
            CLRBIT(ADCSRA, ADEN);
            break;
        }
    }

    REGFPT(ADMUX, ADC_MUX_MASK, ADC_MUX_SHIFT, IntStr_p->AdcSet.Channel);

    switch (IntStr_p->AdcSet.ClockPrescaler) {
        case ADC_CLOCKPRESCALER_2: {
            REGFPT(ADCSRA, ADC_PS_MASK, ADC_PS_SHIFT, 0);
            break;
        }
        case ADC_CLOCKPRESCALER_4: {
            REGFPT(ADCSRA, ADC_PS_MASK, ADC_PS_SHIFT, 1);
            break;
        }
        case ADC_CLOCKPRESCALER_6: {
            REGFPT(ADCSRA, ADC_PS_MASK, ADC_PS_SHIFT, 2);
            break;
        }
        case ADC_CLOCKPRESCALER_8: {
            REGFPT(ADCSRA, ADC_PS_MASK, ADC_PS_SHIFT, 3);
            break;
        }
        case ADC_CLOCKPRESCALER_16: {
            REGFPT(ADCSRA, ADC_PS_MASK, ADC_PS_SHIFT, 4);
            break;
        }
        case ADC_CLOCKPRESCALER_32: {
            REGFPT(ADCSRA, ADC_PS_MASK, ADC_PS_SHIFT, 5);
            break;
        }
        case ADC_CLOCKPRESCALER_64: {
            REGFPT(ADCSRA, ADC_PS_MASK, ADC_PS_SHIFT, 6);
            break;
        }
        case ADC_CLOCKPRESCALER_128: {
            REGFPT(ADCSRA, ADC_PS_MASK, ADC_PS_SHIFT, 7);
            break;
        }
    }

    switch (IntStr_p->AdcSet.Resolution) {
        case ADC_RESOLUTION_10: {
            break;
        }
    }

    switch (IntStr_p->AdcSet.DataAlign) {
        case ADC_DATAALIGN_RIGHT: {
            CLRBIT(ADMUX, ADLAR);
            break;
        }
        case ADC_DATAALIGN_LEFT: {
            SETBIT(ADMUX, ADLAR);
            break;
        }
    }

    switch (IntStr_p->AdcSet.Reference) {
        case ADC_REF_AREF: {
            REGFPT(ADMUX, ADC_REF_MASK, ADC_REF_SHIFT, 0);
            break;
        }
        case ADC_REF_AVCC: {
            REGFPT(ADMUX, ADC_REF_MASK, ADC_REF_SHIFT, 1);
            break;
        }
        case ADC_REF_2560MV: {
            REGFPT(ADMUX, ADC_REF_MASK, ADC_REF_SHIFT, 3);
            break;
        }
    }

    switch (IntStr_p->AdcSet.IntEn) {
        case ENABLE: {
            SETBIT(ADCSRA, ADIE);
            break;
        }
        case DISABLE: {
            CLRBIT(ADCSRA, ADIE);
            break;
        }
    }

    switch (IntStr_p->AdcSet.ConvMode) {
        case ADC_CONVMODE_SUCCESIVELY: {
            SETBIT(ADCSRA, ADFR);
            break;
        }
        case ADC_CONVMODE_TRIGERED: {
            CLRBIT(ADCSRA, ADFR);
            break;
        }
    }

    return 0;
}

uint8_t adc0_en(AdcIntStr_t *IntStr_p, uint8_t en) {
    switch (en) {
        case ENABLE: {
            IntStr_p->AdcSet.AdcConvEn = ENABLE;
            SETBIT(ADCSRA, ADEN);
            return 0;
        }
        case DISABLE: {
            IntStr_p->AdcSet.AdcConvEn = DISABLE;
            CLRBIT(ADCSRA, ADEN);
            return 0;
        }
        default:
            return 1;
    }
}

uint8_t adc0_trig(AdcIntStr_t *IntStr_p) {
    if (!CHKBIT(ADCSRA, ADSC)) {
        SETBIT(ADCSRA, ADSC);
        return 0;
    }
    else {
        // adc is still in conversion progress
        return 1;
    }
}

uint8_t adc0_isDone(AdcIntStr_t *IntStr_p) {
    return !CHKBIT(ADCSRA, ADSC);
}

uint8_t adc0_getConv(AdcIntStr_t *IntStr_p, void *data_p) {
    *((uint16_t*)data_p) = ADC;
    return 0;
}
