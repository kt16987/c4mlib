/**
 * @file twi.h
 * @author LiYu87
 * @brief 宣告 twi 暫存器操作函式原型。
 * @date 2019.01.25
 */

#ifndef C4MLIB_HARDWARE_TWI_H
#define C4MLIB_HARDWARE_TWI_H

/**
 * @defgroup hw_twi_func hardware twi functions
 */

/* Public Section Start */
/**
 * @brief twi flag put 函式
 *
 * @ingroup hw_twi_func
 * @param LSByte 暫存器編號。
 * @param Mask   遮罩。
 * @param Shift  向左位移。
 * @param Data   寫入資料。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 LSByte 錯誤。
 *   - 4：參數 Mask 錯誤。
 *   - 5：參數 Shift 超出範圍。
 */
char TWI_fpt(char LSByte, char Mask, char Shift, char Data);

/**
 * @brief twi flag get 函式
 *
 * @ingroup hw_twi_func
 * @param LSByte 暫存器編號。
 * @param Mask   遮罩。
 * @param Shift  向右位移。
 * @param Data_p 資料指標。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 LSByte 錯誤。
 *   - 4：參數 Mask 錯誤。
 *   - 5：參數 Shift 超出範圍。
 */
char TWI_fgt(char LSByte, char Mask, char Shift, void *Data_p);

/**
 * @brief twi put 函式
 *
 * @ingroup hw_twi_func
 * @param LSByte 暫存器編號。
 * @param Bytes  資料大小。
 * @param Data_p 資料指標。
 * @return char  錯誤代碼：
 *   - 0：成功無誤。
 *   - 3：參數 Bytes 錯誤。
 */
char TWI_put(char LSByte, char Bytes, void *Data_p);

/**
 * @brief twi get 函式
 *
 * @ingroup hw_twi_func
 * @param LSByte 暫存器編號。
 * @param Bytes  資料大小。
 * @param Data_p 資料指標。
 * @return char  錯誤代碼：
 *   - 0：成功無誤。
 *   - 3：參數 Bytes 錯誤。
 */
char TWI_get(char LSByte, char Bytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_HARDWARE_TWI_H
