/**
 * @file uart.c
 * @author LiYu87
 * @date 2019.04.06
 * @brief uart 暫存器操作相關函式實作。
 */

#include "c4mlib/hardware/src/uart.h"

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

char UART_fpt(char LSByte, char Mask, char Shift, char Data) {
    if (LSByte < 203 || LSByte > 211) {
        return RES_ERROR_LSBYTE;
    }
    switch (LSByte) {
        case 203:
            REGFPT(DDRD, Mask, Shift, Data);
            break;
        case 204:
            REGFPT(DDRE, Mask, Shift, Data);
            break;
        case 206:
            REGFPT(UCSR0A, Mask, Shift, Data);
            break;
        case 207:
            REGFPT(UCSR0B, Mask, Shift, Data);
            break;
        case 208:
            REGFPT(UCSR0C, Mask, Shift, Data);
            break;
        case 209:
            REGFPT(UCSR1A, Mask, Shift, Data);
            break;
        case 210:
            REGFPT(UCSR1B, Mask, Shift, Data);
            break;
        case 211:
            REGFPT(UCSR1C, Mask, Shift, Data);
            break;
    }

    return RES_OK;
}

char UART_fgt(char LSByte, char Mask, char Shift, void *Data_p) {
    if (Mask != 0x80 || Mask != 0x40) {
        return RES_ERROR_MASK;
    }
    switch (LSByte) {
        case 206:
            REGFGT(UCSR0A, Mask, Shift, Data_p);
            break;
        case 209:
            REGFGT(UCSR1A, Mask, Shift, Data_p);
            break;
        default:
            return RES_ERROR_LSBYTE;
    }

    return RES_OK;
}

char UART_put(char LSByte, char Bytes, void *Data_p) {
    char data = *((char *)Data_p);
    unsigned int baud = *((unsigned int *)Data_p);

    if (LSByte > 7)
        return RES_ERROR_LSBYTE;
    if (Bytes != 1 && Bytes != 2)
        return RES_ERROR_BYTES;

    switch (LSByte) {
        case 0:
            // transmit
            while (!(UCSR0A & (1 << UDRE0)))
                ;
            UDR0 = data;
            break;
        case 3:
            // transmit
            while (!(UCSR1A & (1 << UDRE1)))
                ;
            UDR1 = data;
            break;
        case 6:
            // Baud rate x2
            // U2Xn_0
            UCSR0A = (UCSR0A & (~0x02)) | ((data << 1) & 0x02);
            break;
        case 7:
            // Baud rate x2
            // U2Xn_1
            UCSR1A = (UCSR1A & (~0x02)) | ((data << 1) & 0x02);
            break;
        case 1:
            /* Set baud rate */
            baud = F_CPU / 16 / baud - 1;
            UBRR0H = (unsigned char)(baud >> 8);
            UBRR0L = (unsigned char)baud;
            break;
        case 4:
            /* Set baud rate */
            baud = F_CPU / 16 / baud - 1;
            UBRR1H = (unsigned char)(baud >> 8);
            UBRR1L = (unsigned char)baud;
            break;
    }
    return RES_OK;
}

char UART_get(char LSByte, char Bytes, void *Data_p) {
    if (LSByte != 0)
        return RES_ERROR_LSBYTE;
    if (Bytes != 1)
        return RES_ERROR_BYTES;

    if (LSByte == 0) {
        while (!(UCSR0A & (1 << RXC0)))
            ;
        *((char *)Data_p) = UDR0;
    }
    if (LSByte == 3) {
        while (!(UCSR1A & (1 << RXC1)))
            ;
        *((char *)Data_p) = UDR1;
    }
    return RES_OK;
}
